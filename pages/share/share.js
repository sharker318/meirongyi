const app = getApp()
let url = app.globalData.url;
let imgurl = url + '/assets/mini_program_img/';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgurl: imgurl,
    group_info: null,
    isDone: false,
    open_num: 0,
    countNum: 0,
    canvasImg: '',
    canvasQr: '',
    canvasAvatar: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.setData({
      group_id: options.group_id
    })
    this.shareGroupInfo();
    this.watchDownimg()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    var group_info = this.data.group_info;
    return {
      title: group_info.goods_name,
      path: '/pages/joincollage/joincollage?group_id=' + group_info.group_id
    }
  },

  //下载图片
  downloadImg(Url, imgtype) {
    var that = this
    wx.downloadFile({
      url: Url,
      success(res) {
        if (imgtype == 1) {
          that.setData({
            canvasImg: res.tempFilePath
          })
        } else if (imgtype == 2) {
          that.setData({
            canvasAvatar: res.tempFilePath
          })
        } else {
          that.setData({
            canvasQr: res.tempFilePath
          })
        }
        console.log(res.tempFilePath)
      }
    })


  },

  //监听图片下载情况
  watchDownimg() {
    var that = this
    var timer = setInterval(function() {
      var canvasImg = that.data.canvasImg
      var canvasQr = that.data.canvasQr
      var canvasAvatar = that.data.canvasAvatar
      if (canvasQr != '' && canvasAvatar != '' && canvasImg != '') {
        that.drawcanvas()
        clearInterval(timer)
      }
    }, 500)
  },

  drawcanvas: function() {
    var _this = this
    const ctx = wx.createCanvasContext('myCanvas')
    ctx.fillStyle = "rgb(203, 14, 14)";
    ctx.fillRect(0, 0, 350, 40);
    ctx.drawImage(this.data.canvasImg, 55, 110, 240, 240)
    ctx.setFontSize(16)
    ctx.fillStyle = '#fff'
    ctx.setTextAlign('center')
    ctx.fillText('我在美物上发现了一个宝贝，分享给你', 175, 27)
    var ava_width = 40; //头像宽度
    var ava_height = 40; //头像高度
    var ava_x = 20;
    var ava_y = 50;
    var goodsname = this.data.group_info.goods_name.slice(0, 10) + '...'
    ctx.save();
    ctx.beginPath();
    ctx.arc(ava_width / 2 + ava_x, ava_height / 2 + ava_y, ava_width / 2, 0, Math.PI * 2, false);
    ctx.clip();
    ctx.drawImage(this.data.canvasAvatar, ava_x, ava_y, ava_width, ava_height);
    ctx.restore();
    ctx.setFontSize(14)
    ctx.fillStyle = '#222'
    ctx.setTextAlign('center')
    ctx.fillText(this.data.group_info.nickname, 90, 78)
    ctx.setFontSize(16)
    ctx.fillStyle = '#222'
    ctx.setTextAlign('left')
    ctx.fillText(goodsname, 30, 390)
    ctx.fillStyle = '#CB0E0E'
    ctx.setFontSize(14)
    ctx.setTextAlign('left')
    ctx.fillText('￥' + this.data.group_info.collage_price, 30, 430)
    ctx.fillStyle = '#999'
    ctx.setFontSize(10)
    ctx.setTextAlign('left')
    ctx.fillText('￥' + this.data.group_info.goods_price, 30, 445)
    ctx.fillStyle = '#999'
    ctx.setTextAlign('left')
    ctx.fillText('长按二维码，查看详情', 30, 460)
    ctx.drawImage(this.data.canvasQr, 220, 370, 100, 100)
    ctx.draw(true, function() {
      wx.canvasToTempFilePath({
        fileType: 'jpg',
        canvasId: 'myCanvas',
        success: function(res) {
          wx.hideLoading()
          _this.setData({
            canvaspath: res.tempFilePath,
            isDone: true,
          })
        }
      })
    })
  },
  //保存图片到手机
  saveCanvas() {
    var that = this;
    wx.saveImageToPhotosAlbum({
      filePath: this.data.canvaspath,
      success: function(res) {
        console.log(res)
        wx.showToast({
          title: '保存成功',
        })
      },
      fail: function(res) {
        wx.showToast({
          title: '保存失败',
          icon: 'none'
        })
        if (res.errMsg.indexOf('cancel') == -1) {
          wx.openSetting({
            success: (res) => {}
          })
        }
      }
    })
  },

  /**
   * 获取组团信息
   */
  shareGroupInfo() {
    wx.showLoading({
      title: '图片生成中',
    })
    app.get('/api/Collage/shareGroupInfo', {
      group_id: this.data.group_id
    }, (res) => {
      console.log(res)

      if (res.data.code == 200) {
        this.downloadImg(res.data.data.image, 1) //保存图片分享用
        this.downloadImg(res.data.data.avatar, 2)
        this.downloadImg(res.data.data.qrcode, 3)
        this.setData({
          group_info: res.data.data,
        })
      }
    }, null, true)
  }
})