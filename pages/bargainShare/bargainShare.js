
// pages/bargain/bargain.js
const app = getApp()
let url = app.globalData.url;
let imgurl = url + '/assets/mini_program_img/';


Page({

  /**
   * 页面的初始数据
   */
  data: {
    cut_id: '',
    goods: '',
    standard: false,
    showKan: false,
    leader: '',
    cut_price: '',
    timeDown: ''
  },

  // 获取砍价产品详情
  getDetail() {
    var that = this
    app.post('/api/bargain/bargainDetail', { id: that.data.cu_id }, res => {
      if (res.data.code == 200) {
        that.setData({
          goods: res.data.data
        })
        that.downTime()
      }
    })
  },

  // 参与砍价
  joinKan() {
    var that = this
    app.post('/api/bargain/attend', { cu_id: that.data.cu_id }, res => {
      if (res.data.code == 200) {
        that.setData({
          showKan: true,
          cut_price: res.data.data.price
        })
        wx.showToast({
          title: res.data.msg,
          icon: 'success'
        })
        that.getDetail()
      } else {
        wx.showToast({
          title: res.data.msg,
          icon: 'success'
        })
      }
    })
  },

  // 展示规则的方法
  showStantard() {
    var that = this
    that.setData({
      standard: !that.data.standard
    })
  },

  // 关闭砍价提示
  closeKan() {
    var that = this
    that.setData({
      showKan: false
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    that.setData({
      cut_id: options.cut_id,
      cu_id: options.cu_id,
      goods_id: options.goods_id
    })
    if (options.leader) {
      that.setData({
        leader: options.leader
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getDetail()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  //立即购买
  buyNow() {
    var that = this
    //先添加到购物车
    wx.navigateTo({
      url: '/pages/goods_detail/goods_detail?id=' + that.data.goods_id,
    })
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: '砍价分享',
      path: '/pages/bargainShare/bargainShare?cu_id=' + this.data.cu_id
    }
  },



  //  活动倒计时方法
  downTime() {
    var that = this
    console.log(that.data.goods)
    console.log(that.data.goods.countdown)
    console.log(that.data.goods.countdown.day)
    var totalSecond = (that.data.goods.countdown.day * 24 * 60 * 60) + (that.data.goods.countdown.hour * 60 * 60) + (that.data.goods.countdown.min * 60) + that.data.goods.countdown.sec
    var interval = setInterval(function () {
      // 秒数
      var second = totalSecond;
      // 天数位
      var day = Math.floor(second / 3600 / 24);
      var dayStr = day.toString();
      if (dayStr.length == 1) dayStr = '0' + dayStr;

      // 小时位
      var hr = Math.floor((second - day * 3600 * 24) / 3600);
      var hrStr = hr.toString();
      if (hrStr.length == 1) hrStr = '0' + hrStr;

      // 分钟位
      var min = Math.floor((second - day * 3600 * 24 - hr * 3600) / 60);
      var minStr = min.toString();
      if (minStr.length == 1) minStr = '0' + minStr;

      // 秒位
      var sec = second - day * 3600 * 24 - hr * 3600 - min * 60;
      var secStr = sec.toString();
      if (secStr.length == 1) secStr = '0' + secStr;

      let timeContent = {
        day: dayStr,
        hour: hrStr,
        min: minStr,
        sec: secStr,
      }
      this.setData({
        timeDown: timeContent
      });
      totalSecond--;
      if (totalSecond < 0) {
        clearInterval(interval);
        wx.showToast({
          title: '活动已结束',
        });
        let timeContent = {
          day: '00',
          hour: '00',
          min: '00',
          sec: '00',
        }
        this.setData({
          timeDown: timeContent
        });
      }
    }.bind(this), 1000);
  }

})