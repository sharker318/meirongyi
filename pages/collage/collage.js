//获取应用实例
const app = getApp()

//获取应用实例
let ERR_OK = 200;
let url = app.globalData.url;
let imgcomurl = url + '/assets/img/';
import auth from '../../auth/auth.js'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    url: url,
    banner: '',
    list:'',
    swiperCurrent: 0,
    page_num:1,
    page_size:10,
    is_loading:false,
    id: '',
  },

  swiperChange: function(e) {
    this.setData({
      swiperCurrent: e.detail.current
    })
  },

  getList(callback) {
    app.showToast({
      identifier: 'collage_list',
      title: '加载中',
      icon: 'loading'
    })
    var that =this;
    if (this.data.is_loading){
        return ;
    }
    this.setData({
      is_loading : true,
    })
    app.get('/api/index/getRecommendGoodsById', {
        recommend_id: this.data.id,
        page_num:this.data.page_num,
        page_size:this.data.page_size
    }, function(res) {
      console.log(res)
      if(res.data.code == 200){
        app.hideToast();
        var list = that.data.list;
        if (list.length > 0 && res.data.data.length >0){
            list = list.concat(res.data.data.list);
        } else if (list.length == 0) {
          list = res.data.data.list;
        }
        that.setData({
          list: list,
          is_loading: false,
          page_num: ++that.data.page_num
        })

      typeof callback == 'function' ? callback() : '';
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.setData({
      id: options.id
    })
    this.getList()
    // console.log(options)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    this.setData({
       'page_num': 1,
       'list' : []
    });
    this.getList(function () {
      wx.stopPullDownRefresh()
    });
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    this.getList();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
  
})