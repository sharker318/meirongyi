//获取应用实例
const app = getApp()
let url = app.globalData.url;
let imgurl = url + '/assets/mini_program_img/';
import auth from '../../auth/auth.js'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgurl: imgurl,
    isEdit: false,
    cart: '',
    selsectAll: false,
    amount: 0,
    noData: false,
    guessYourFavour: null
  },

  pay() {
    var list = this.data.cart.list
    var num = 0
    for (var i = 0; i < list.length; i++) {
      if (list[i].checked == 1) {
        num++
      }
    }
    if (num == 0) {
      wx.showToast({
        title: '请至少选择一件商品',
        icon: 'none'
      })
    } else {
      wx.navigateTo({
        url: '../pay/pay',
      })
    }
  },

  //显示删除操作
  deleOp() {
    this.setData({
      isEdit: !this.data.isEdit
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    auth()
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.getList()
    this.guessYourFavour()
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },

  //获取购物车
  getList() {
    wx.showLoading({
      title: '加载中',
    })
    var that = this
    var allcheck = true
    app.get('/api/cart/getList', {}, function(res) {
      wx.hideLoading()
      // console.log(res)
      if (res.data.code == 200) {
        var cart = res.data.data
        if (cart.list != '') {
          for (var i = 0; i < cart.list.length; i++) {
            if (cart.list[i].checked == 0) {
              allcheck = false
              break;
            }
          }
          that.setData({
            cart: cart,
            selsectAll: allcheck,
            noData: false,
          })
          that.countMoney()
        } else {
          that.setData({
            noData: true,
          })
        }
      } else {
        wx.showToast({
          title: res.data.msg,
          icon: 'none'
        })
      }
    })
  },

  //更换选中状态
  checkState(e) {
    var cart = this.data.cart
    var index = e.target.id
    if (cart.list[index].checked == 0) {
      cart.list[index].checked = 1
    } else {
      cart.list[index].checked = 0
    }
    this.setData({
      cart: cart
    })
    this.sendcheck(index, 0) //后台同步选中状态
    this.countMoney()
    var seleNum = 0
    for (var i = 0; i < cart.list.length; i++) {
      if (cart.list[i].checked == 1) {
        seleNum++
      }
    }
    if (seleNum == cart.list.length) {
      this.setData({
        selsectAll: 1
      })
    } else {
      this.setData({
        selsectAll: 0
      })
    }
  },

  //后台同步选中状态
  sendcheck(index, allcheck) {
    var cart = this.data.cart
    var checkID = ''
    var checked = ''
    if (allcheck == 0) {
      checkID = cart.list[index].id
      checked = cart.list[index].checked
    } else {
      for (var i = 0; i < cart.list.length; i++) {
        checkID = checkID + cart.list[i].id + ','
      }
      checkID = checkID.slice(0, checkID.length - 1)
      checked = this.data.selsectAll
      if (checked == false) {
        checked = 0
      } else {
        checked = 1
      }

    }
    console.log(checkID, checked)
    app.get('/api/cart/changeChecked', {
      cart_id: checkID,
      checked: checked,
    }, function(res) {
      console.log(res)
    })
  },
  //数量加减
  changeNum(e) {
    // console.log(e)
    var cart = this.data.cart
    var op = e.currentTarget.dataset.op
    var index = e.currentTarget.id
    if (op == 1) {
      if (cart.list[index].number <= parseInt(cart.list[index].stock)) {
        cart.list[index].number++
      } else {
        wx.showToast({
          title: '数量已超过库存！',
          icon: 'none'
        })
      }
    } else {
      if (cart.list[index].number != 1) {
        cart.list[index].number--
      } else {
        wx.showToast({
          title: '数量最少为1！',
          icon: 'none'
        })
      }
    }
    this.setData({
      cart: cart
    })
    this.countMoney()
    this.sendNum(cart.list[index].id, cart.list[index].number) //后台同步购物车数量 
  },

  //后台同步购物车数量 
  sendNum(id, num) {
    app.get('/api/cart/changeNum', {
      cart_id: id,
      num: num
    }, function(res) {
      console.log(res)
    })
  },
  //计算价格
  countMoney() {
    var cart = this.data.cart
    var amount = 0
    // console.log(cart)
    for (var i = 0; i < cart.list.length; i++) {
      if (cart.list[i].checked == 1) {
        var money = cart.list[i].real_price * 100 * cart.list[i].number / 100
        amount += money
      }
    }
    this.setData({
      amount: amount
    })
  },

  //全选
  selAll() {
    this.setData({
      selsectAll: !this.data.selsectAll
    })
    var that = this
    var cart = this.data.cart
    for (var i = 0; i < cart.list.length; i++) {
      if (cart.list[i].checked != this.data.selsectAll) {
        cart.list[i].checked = this.data.selsectAll
      }
    }
    this.setData({
      cart: cart
    })
    this.countMoney()
    this.sendcheck(0, 1)
  },

  //删除商品
  deleList() {
    var that = this
    var cart = this.data.cart
    var ids = ''
    for (var i = 0; i < cart.list.length; i++) {
      if (cart.list[i].checked == 1) {
        ids = ids + cart.list[i].id + ','
      }
    }
    ids = ids.slice(0, ids.length - 1)
    app.get('/api/cart/delCart', {
      cart_id: ids
    }, function(res) {
      if (res.data.code == 200) {
        wx.showToast({
          title: '删除成功',
        })
        that.getList()
      }
    })
  },

  //猜你喜歡接口數據
  guessYourFavour(){
    var that = this
    app.get('/api/cart/guessYourFavour',{},function(res){
      console.log(res.data.data)
      that.setData({
        guessYourFavour: res.data.data
      })
    })
  }

})