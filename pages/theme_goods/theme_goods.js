const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    goods: '',
    showPic: false,
    pageNum: 1,
    pageSize: 10,
    tag: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    wx.setNavigationBarTitle({
      title: options.title,
    })
    this.setData({
      tag: options.id
    })
    this.getlist(options.id)
  },

  getlist(tag) {
    console.log(tag)
    var that = this
    var list = that.data.goods
    app.get('/api/index/getRecommendGoodsById', {
      recommend_id: tag,
      page_num: this.data.pageNum,
      page_size: this.data.pageSize
    }, function(res) {
      wx.hideLoading()
      console.log(res)
      if (res.data.code == 200) {
        if (res.data.data.list == ''){
          wx.showToast({
            title: '已没有更多了',
            icon:'none'
          })
        }
        if (list == '') {
          that.setData({
            goods: res.data.data.list
          })
        } else {
          that.setData({
            goods: list.concat(res.data.data.list)
          })
        }
      }
    })
  },

  //跳转到详情页
  toDetail(e) {
    wx.navigateTo({
      url: '../goods_detail/goods_detail?id=' + e.currentTarget.id,
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

    var that = this;
    wx.showToast({
      title: '正在刷新',
      icon: 'loading',
      duration: 1000,
      success() {
        that.setData({
          pageNum: 1,
          goods: ''
        })
        that.getlist(that.data.tag);
        wx.stopPullDownRefresh()
      }
    })
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    wx.showLoading({
      title: '加载中',
    })
    var that = this
    that.setData({
      pageNum: this.data.pageNum + 1
    })
    that.getlist(this.data.tag)
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  //图片懒加载
  showPicture: function() {
    let that = this
    let show = that.data.showPic
    let animation = wx.createAnimation({
      duration: 300,
      timingFunction: 'ease-in',
      delay: 0
    })
    show = true
    animation.opacity(1).step()
    that.setData({
      showPic: show,
      animationData: animation.export()
    })
  },
})