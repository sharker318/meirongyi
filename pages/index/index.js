//获取应用实例
const app = getApp()

//获取应用实例
let ERR_OK = 200;
let url = app.globalData.url;
let portUrl = app.globalData.portUrl;
let imgurl = url + '/assets/mini_program_img/';
import auth from '../../auth/auth.js'
Page({
  data: {
    url: url,
    portUrl: portUrl,
    imgurl: imgurl,
    banner: '',
    userId: '',
    tags: '',
    collage_leader: '',
    currentTab: 0,
    recommends: '',
    swiperCurrent: 0,
    showPic: false,
    category: '',
    themegoods: '',
    banner: ['/images/index/banner.jpg', '/images/index/banner.jpg']
  },

  onLoad(options) {
    auth();
    this.getBanner();
    this.getRecommendGoods();
    // this.getTages();
    // this.getRecommends();
    // 完成邀请好友的绑定
    
  },

  onShow(options){
    if (options) {
      var data = { user_id: options.userId }
      app.get('/api/ucenter/belong', data, function (res) {
        console.log(res)
      })
    }
    this.checkPartern()
  },

  swiperChange: function(e) {
    this.setData({
      swiperCurrent: e.detail.current
    })
  },
  //获取banner
  getBanner() {
    var that = this
    app.get('/api/index/getBanner', {}, function(res) {
      if (res.data.code == 200) {
        that.setData({
          banner: res.data.data
        })
      } else {
        wx.showToast({
          title: res.data.msg,
          icon: 'none'
        })
      }
    })
  },

  //获取主题及商品
  getRecommendGoods() {
    var that = this
    app.get('/api/index/getRecommendGoods', {}, function(res) {
      if (res.data.code == 200) {
        console.log(res)
        that.setData({
          themegoods: res.data.data
        })
      }
    })
  },

  //获取标签
  // getTages() {
  //   var that = this
  //   app.get('/api/index/getLabel', {}, function(res) {
  //     // console.log(res)
  //     if (res.data.code == 200) {
  //       that.setData({
  //         tags: res.data.data
  //       })
  //     } else {
  //       wx.showToast({
  //         title: res.data.msg,
  //       })
  //     }

  //   })
  // },
  //获取主题商品
  // getRecommends() {
  //   var that = this
  //   app.get('/api/index/getRecommendGoods', {}, function(res) {
  //     if (res.data.code == 200) {
  //       that.setData({
  //         recommends: res.data.data
  //       })
  //       console.log(res.data.data)
  //     } else {
  //       wx.showToast({
  //         title: res.data.msg,
  //       })
  //     }
  //   })

  // },
  // tab栏的切换
  // tabChange: function(e) {
  //   // console.log(e)
  //   this.setData({
  //     currentTab: e.currentTarget.id,
  //   })
  //   app.globalData.activeTab = e.currentTarget.id,
  //     setTimeout(function() {
  //       wx.switchTab({
  //         url: '../classify/classify'
  //       })
  //     }, 300)
  // },
  //跳转到详情页
  toDetail(e) {
    wx.navigateTo({
      url: '../goods_detail/goods_detail?id=' + e.currentTarget.id,
    })
    
  },
  //跳转到搜索
  toSearch() {
    wx.navigateTo({
      url: '../search/search',
    })
  },

  //图片懒加载
  showPicture: function() {
    let that = this
    let show = that.data.showPic
    let animation = wx.createAnimation({
      duration: 300,
      timingFunction: 'ease-in',
      delay: 0
    })
    show = true
    animation.opacity(1).step()
    that.setData({
      showPic: show,
      animationData: animation.export()
    })
  },
  showToast(){
    wx.showToast({
      title: '暂未开放，敬请期待',
      icon:'none'
    })
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    var userId = wx.getStorageSync('userId')
    return {
      title: '邀请好友',
      path: '/pages/index/index?userId=' + userId
    }
  },

  // 判断是否已经成为了合伙人
  checkPartern(){
    var that = this
    app.get('/api/common/partner','',function(res){
      console.log(res)
      that.setData({
        collage_leader: res.data.data.collage_leader
      })
    })
  },

  // 申请合伙人操作
  runPartern(){
    if (this.data.collage_leader){
      wx.showToast({
        title: '您已经成为合伙人！',
        icon: 'none'
      })
    }else{
      wx.navigateTo({
        url: '/pages/user/partner/partner',
      })
    }
  }


})