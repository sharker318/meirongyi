// pages/logistics/logistics.js
const app = getApp()
let url = app.globalData.url;
let imgurl = url + '/assets/mini_program_img/';
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var id = options.id;
    var op = options.type;
    this.getDeliveryInfo(id,op);
  },

  getDeliveryInfo: function(id,op){
    var that = this;
    wx.showLoading({
      title: '加载中',
    })
    if (op==0) {  //订单查物流
  
      app.get('/api/order/getDeliveryInfo',{order_id: id},function(res) {
        wx.hideLoading();
        if (res.data.code == 200) {
          that.setData({
            deliveryInfo: res.data.data
          })
        } else {
          wx.showToast({
            title: '网络错误',
            image: '../../images/warning.png'
          })
        }
      })
    } else if (op==1) {   //售后查物流
     
      app.get('/api/aftersale/getDeliveryInfo', { aftersale_id: id }, function (res) {
        wx.hideLoading();
        if(res.data.code == 200) {
            that.setData({
              deliveryInfo: res.data.data
            })
        } else {
          wx.showToast({
            title: '网络错误',
            image: '../../images/warning.png'
          })
        }
      })
    }

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})