const WxParse = require('../../utils/wxParse/wxParse.js')
const app = getApp()
let url = app.globalData.url;
let imgurl = url + '/assets/mini_program_img/';
import auth from '../../auth/auth.js'
Page({
  data: {
    currentTab: 'v1',
    imgurl: imgurl,
    id: '',
    startPing: false,
    goods: '',
    collect: false,
    num: 1,
    delet: 0,
    imgsNum: 0,
    selectid: '',
    buyType: 0, //0：单人，1：拼单
    single: '',
    group_id: '',
    isSelect: false,
    sub_normal_group_list: '',
    grouptotal: 0,
    popUp: false,
    showConfirm: false,
    showAcitivity: false,
    showTab: false,
    pic_index: 1,
    playVideo: false,
    spec: '',
    normal_group_list: '',
    self_group_list: '',
    selectSpec: '',
    selectAct: '',
    selectImg: '',
    selectPrice: '',
    mulType: false,
    comments: '',
    joingroup: false,
    sheargoup: 0,
    tabs: [{
        id: 'v1',
        title: '商品'
      },
      {
        id: 'v2',
        title: '评价'
      },
      {
        id: 'v3',
        title: '详情'
      }
    ],
  },

  // tab栏的切换
  tabChange: function(e) {
    var tabId = e.currentTarget.dataset.id
    this.setData({
      currentTab: tabId,
    })
  },

  //图片索引切换
  showId(e) {
    var picIdx = e.detail.current + 1
    this.setData({
      pic_index: picIdx
    })
  },

  showPop() {
    this.setData({
      showConfirm: true,
      popUp: true,
      popmask: true
    })
  },



  //关闭弹窗
  closeConfirm() {
    this.setData({
      showConfirm: false,
      showAcitivity: false,
      popUp: false,
      startPing: false,
      isShare: false,
      popmask: false,
      friendship: false
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    auth()
    this.getUserInfo()
    this.setData({
      group_id: options.group_id
    })
    this.getinfo(options.group_id)
  },

  getinfo(group_id) {
    var that = this
    app.showToast({
      'identifier': 'share_join',
      'title': '加载中',
      'icon': 'loading'
    })
    //获取商品数据
    app.get('/api/collage/shareJoin', {
      group_id: group_id
    }, function(res) {
      wx.hideLoading()
      var goods = res.data.data
      if (res.data.code == 200) {
        that.setData({
          goods: goods,
          comments: goods.comments,
          lifecycle: goods.lifecycle
          // selectAct: goods.activity[0].name,
          // collect: goods.collect
        })

        that.downloadImg(that.data.goods.photo_images[0], 1) //保存图片分享用
        that.downloadImg(that.data.goods.qrcode, 3)

        WxParse.wxParse('content', 'html', goods.content, that, 0);
        if (goods.lifecycle) {
          that.getTimer(goods.lifecycle)
        }
        if (goods.spec_group) {
          that.setData({
            mulType: true
          })
        } else {
          that.setData({
            selectid: goods.products.id
          })
        }
        if (goods.video) {
          that.setData({
            imgsNum: goods.photo_images.length + 1
          })
        } else {
          that.setData({
            imgsNum: goods.photo_images.length
          })
        }
        if(goods.is_finish == 1){
          return wx.showModal({
            title: '拼团已完成！',
            content: '您可以找其他小伙伴拼团！',
            showCancel:false,
            success:function(){
              wx.switchTab({
                url: '/pages/index/index',
              });
            }
          });
        }

        if (goods.overtime == 1) {
          return wx.showModal({
            title: '拼团已超时！',
            content: '您可以尝试着重新去拼团！',
            showCancel: false,
            success: function () {
              wx.switchTab({
                url: '/pages/index/index',
              });
            }
          });
        }

      } else {
        app.showToast({
          title: '网络错误',
          icon: 'warning'
        })
      }

    })

  },

  //处理获取的规格
  getSpec(spec_group) {
    var that = this
    var spec = spec_group
    for (var i = 0; i < spec.length; i++) {
      spec[i].checked = 0
    }
    that.setData({
      spec: spec
    })
    that.getselText()
  },

  //选择规格
  select_spec(e) {
    // console.log(e)
    var that = this
    var spec = this.data.spec
    var fidx = e.currentTarget.dataset.fidx
    var checkId = e.currentTarget.dataset.idx
    var selText = ''
    spec[fidx].checked = checkId
    that.setData({
      spec: spec
    })
    that.getselText()
  },

  //拼接文字
  getselText() {
    var that = this
    var spec = this.data.spec
    var products = this.data.goods.products
    var selText = ''
    for (var i = 0; i < spec.length; i++) {
      var itemCheckid = spec[i].checked
      selText = selText + spec[i].value[itemCheckid] + ','
    }
    selText = selText.slice(0, selText.length - 1)
    that.setData({
      selectSpec: selText,
      selectid: products[selText].id, //获取规格id
      selectImg: products[selText].image, //规格图片
      selectPrice: products[selText].real_price, //规格价格
    })
    if (this.data.buyType == 0) { //单买价
      that.setData({
        selectPrice: products[selText].real_price
      })
    } else { //拼团价
      that.setData({
        selectPrice: products[selText].collage_price
      })
    }
    // console.log(products[selText].id)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },

  //选择活动
  chooseAct() {
    var that = this
    that.setData({
      showAcitivity: true,
      showConfirm: true,
      popUp: true,
      playVideo: false,
    })
  },

  //数量加减
  changeNum(e) {
    var that = this
    var num = this.data.num
    var op = e.currentTarget.dataset.op
    if (op == 1) {
      num++
    } else {
      if (num == 1) {
        return
      } else {
        num--
      }
    }
    that.setData({
      num: num
    })
  },

  //查看大图
  previewImage() {
    var photos = this.data.goods.photo_images
    var index = this.data.pic_index - 2
    wx.previewImage({
      current: photos[index],
      urls: photos
    })
  },

  //播放、退出视频
  playVideo() {
    var that = this
    that.setData({
      playVideo: true
    })
  },

  stopPlay() {
    var that = this
    that.setData({
      playVideo: false
    })
  },
  //顶部tab显隐
  showTab(e) {
    // console.log(e)
    if (e.detail.scrollTop >= 100) {
      this.setData({
        showTab: true
      })
    } else {
      this.setData({
        showTab: false
      })
    }
  },

  //选择促销活动
  select_act(e) {
    this.setData({
      selectAct: e.currentTarget.dataset.act
    })
    this.closeConfirm()
  },

  //拼团
  buyGroup() {
    var goods_id = this.data.goods.goods_id
    var product_id = this.data.selectid
    var num = this.data.num
    var group_id = this.data.group_id
    var paraStr = 'goods_id=' + goods_id + '&product_id=' + product_id + '&number=' + num + '&group_id=' + group_id
    wx.navigateTo({
      url: '../pay/collage_pay/collage_pay?' + paraStr
    })
  },

  //单买
  buySingle() {
    var productid = this.data.selectid
    var goodsid = this.data.goods.goods_id
    var num = this.data.num
    wx.navigateTo({
      url: '../pay/pay?productid=' + productid + '&goodsid=' + goodsid + '&num=' + num,
    })
  },



  //确定
  submit() {
    this.closeConfirm()
    var buyType = this.data.buyType
    if (buyType == 0) {
      this.buySingle()
    } else {
      this.buyGroup()
    }
  },



  //计时器
  getTimer() {
    var that = this
    setInterval(function() {
      var lifecycle = that.data.lifecycle
      var timer = ''
      lifecycle--
      if (lifecycle <= 0) {
        that.setData({
          timer: '00:00:00',
          lifecycle: 0
        })
        return
      } else {
        timer = that.formatSeconds(lifecycle)
        that.setData({
          timer: timer,
          lifecycle: lifecycle
        })
      }
    }, 1000)
  },


  //格式化时间
  formatSeconds(value) {
    var secondTime = parseInt(value); // 秒
    var minuteTime = 0; // 分
    var hourTime = 0; // 小时
    if (secondTime > 60) {
      minuteTime = parseInt(secondTime / 60);
      secondTime = parseInt(secondTime % 60);
      if (minuteTime > 60) {
        hourTime = parseInt(minuteTime / 60);
        minuteTime = parseInt(minuteTime % 60);
      }
    }
    var result = "" + parseInt(secondTime) + "";
    if (secondTime < 10) {
      result = "" + '0' + parseInt(secondTime) + ""
    }
    if (minuteTime < 10) {
      result = "" + '0' + parseInt(minuteTime) + ":" + result;
    } else {
      result = "" + parseInt(minuteTime) + ":" + result;
    }
    if (hourTime < 10) {
      result = "" + '0' + parseInt(hourTime) + ":" + result;
    } else {
      result = "" + parseInt(hourTime) + ":" + result;
    }

    return result;
  },

  goping(e) { //参团
    var that = this
    this.showPop()
    this.getSpec(this.data.goods.spec_group)
    that.setData({
      joingroup: true,
      group_id: e.currentTarget.id,
      buyType: 1
    })
  },
  startgroup() {
    var that = this
    that.setData({
      startPing: true,
      popUp: true,
      popmask: true
    })
  },
  myjoin(e) {
    this.closeConfirm()
    this.buyOp(e)
  },


  inviteFri() {
    wx.navigateTo({
      url: '/pages/share/share',
    })
  },

  //分享按钮
  share() {
    var that = this
    that.downloadImg(that.data.goods.photo_images[0], 1) //保存图片分享用
    that.downloadImg(that.data.avatarUrl, 2)
    that.downloadImg(that.data.goods.qrcode, 3)
    that.setData({
      isShare: true,
      popmask: true
    })
  },

  //分享到朋友圈
  friendCicle() {
    var that = this
    that.drawcanvas()
    that.setData({
      friendship: true,
      isShare: false,
      popmask: true,
    })
  },


  //获取用户头像、昵称

  getUserInfo() {

    var that = this
    that.setData({
      avatarUrl: app.globalData.userInfo.avatar,
      nickName: app.globalData.userInfo.nickname,
    })
    that.downloadImg(that.data.avatarUrl, 2)
  },

  drawcanvas: function() {
    // wx.showLoading({
    //   title: '生成图片中',
    // })
    var _this = this
    const ctx = wx.createCanvasContext('myCanvas')
    ctx.fillStyle = "rgb(203, 14, 14)";
    ctx.fillRect(0, 0, 350, 40);
    ctx.drawImage(this.data.canvasImg, 55, 110, 240, 240)
    ctx.setFontSize(16)
    ctx.fillStyle = '#fff'
    ctx.setTextAlign('center')
    ctx.fillText('我在美物上发现了一个宝贝，分享给你', 175, 27)
    var ava_width = 40; //头像宽度
    var ava_height = 40; //头像高度
    var ava_x = 20;
    var ava_y = 50;
    var goodsname = this.data.goods.name.slice(0, 10) + '...'
    ctx.save();
    ctx.beginPath();
    ctx.arc(ava_width / 2 + ava_x, ava_height / 2 + ava_y, ava_width / 2, 0, Math.PI * 2, false);
    ctx.clip();
    ctx.drawImage(this.data.canvasAvatar, ava_x, ava_y, ava_width, ava_height);
    ctx.restore();
    ctx.setFontSize(14)
    ctx.fillStyle = '#222'
    ctx.setTextAlign('center')
    ctx.fillText(this.data.nickName, 90, 78)
    ctx.setFontSize(16)
    ctx.fillStyle = '#222'
    ctx.setTextAlign('left')
    ctx.fillText(goodsname, 30, 390)
    ctx.fillStyle = '#CB0E0E'
    ctx.setFontSize(14)
    ctx.setTextAlign('left')
    ctx.fillText('￥' + this.data.goods.real_price, 30, 430)
    ctx.fillStyle = '#999'
    ctx.setFontSize(10)
    ctx.setTextAlign('left')
    ctx.fillText('￥' + this.data.goods.origin_price, 30, 445)
    ctx.fillStyle = '#999'
    ctx.setTextAlign('left')
    ctx.fillText('长按二维码，查看详情', 30, 460)
    ctx.drawImage(this.data.canvasQr, 220, 370, 100, 100)
    ctx.draw(true, function() {
      wx.canvasToTempFilePath({
        fileType: 'jpg',
        canvasId: 'myCanvas',
        success: function(res) {
          wx.hideLoading()
          _this.setData({
            canvaspath: res.tempFilePath
          })
          console.log(res.tempFilePath)
        }
      })
    })
  },
  //保存图片到手机
  saveCanvas() {
    var that = this;
    wx.saveImageToPhotosAlbum({
      filePath: this.data.canvaspath,
      success: function(res) {
        that.closeConfirm()
        wx.showToast({
          title: '保存成功',
        })
      },
      fail: function(res) {
        wx.showToast({
          title: '保存失败',
          icon: 'none'
        })
        if (res.errMsg.indexOf('cancel') == -1) {
          wx.openSetting({
            success: (res) => {}
          })
        }
      }
    })
  },
  //记录是单人还是立即团购
  buyOp(e) {
    var op = e.currentTarget.dataset.op
    this.stopPlay()
    this.showPop()
    this.setData({
      buyType: op
    })
    this.getSpec(this.data.goods.spec_group)
  },

  //保存按钮
  saveImg() {
    this.saveCanvas()
  },
  downloadImg(Url, imgtype) {
    var that = this
    wx.downloadFile({
      url: Url,
      success(res) {
        if (imgtype == 1) {
          that.setData({
            canvasImg: res.tempFilePath
          })
        } else if (imgtype == 2) {
          that.setData({
            canvasAvatar: res.tempFilePath
          })

        } else {
          that.setData({
            canvasQr: res.tempFilePath
          })
        }
      }
    })
  },
})