const app = getApp()
let url = app.globalData.url;
let imgurl = url + '/assets/mini_program_img/';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: '',
    noData: '',
    imgurl: imgurl
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.setData({
      group_id: options.group_id
    });
    this.getRow();
  },

  getRow() {
    var that = this
    app.get('/api/collage/collageGroupDetail', {
      group_id: this.data.group_id
    }, function(res) {
      if (res.data.code == 200) {
        console.log(res)
        if (res.data.data != '') {
          that.setData({
            noData: false,
            list: res.data.data
          })
        } else {
          that.setData({
            noData: true,
          })
        }
      } else {
        wx.showToast({
          title: res.data.msg,
          icon: 'none'
        })

      }
    }, null, true);
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    return {
      path: '/pages/joincollage/joincollage?group_id' + this.data.group_id
    }
  }
})