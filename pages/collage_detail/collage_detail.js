const WxParse = require('../../utils/wxParse/wxParse.js')
const app = getApp()
let url = app.globalData.url;
let imgurl = url + '/assets/mini_program_img/';
Page({
  data: {
    currentTab: 'v1',
    imgurl: imgurl,
    id: '',
    startPing: false,
    goods: {},
    collect: false,
    num: 1,
    delet: 0,
    imgsNum: 0,
    selectid: '',
    buyType: 0, //0：单人，1：拼单
    single: '',
    group_id: '',
    isSelect: false,
    sub_normal_group_list: '',
    grouptotal: 0,
    popUp: false,
    showConfirm: false,
    showAcitivity: false,
    showTab: false,
    pic_index: 1,
    playVideo: false,
    spec: '',
    spec: [],
    normal_group_list: [],
    self_group_list: [],
    selectSpec: '',
    selectAct: '',
    selectImg: '',
    selectPrice: '',
    mulType: false,
    comments: '',
    joingroup: false,
    sheargoup: 0,
    is_collage_leader: 0,
    leader_create_collage: 0, //在用户是团长的前提下，判断是以团长身份还是以普通用户身份发起拼团
    group_num: 0,
    limit_num: 0,
    setIntvalIds: {},
    tabs: [{
        id: 'v1',
        title: '商品'
      },
      {
        id: 'v2',
        title: '评价'
      },
      {
        id: 'v3',
        title: '详情'
      }
    ],
  },

  // tab栏的切换
  tabChange: function(e) {
    var tabId = e.currentTarget.dataset.id
    this.setData({
      currentTab: tabId,
    })
  },

  //图片索引切换
  showId(e) {
    var picIdx = e.detail.current + 1
    this.setData({
      pic_index: picIdx
    })
  },

  showPop() {
    this.setData({
      showConfirm: true,
      popUp: true,
      popmask: true
    })
  },



  //关闭弹窗
  closeConfirm() {
    this.setData({
      showConfirm: false,
      showAcitivity: false,
      popUp: false,
      startPing: false,
      isShare: false,
      popmask: false,
      friendship: false
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.getUserInfo()
    this.setData({
      id: options.id
    })
    this.getinfo(options.id);
  },

  getinfo(id) {
    var that = this
    app.showToast({
      'identifier': 'collage_detail',
      'title': '加载中',
      'icon': 'loading'
    })
    //获取商品数据
    app.get('/api/collage/detail', {
      id: id
    }, function(res) {
      console.log(res)
      wx.hideLoading()
      var goods = res.data.data
      if (res.data.code == 200) {

        that.setData({
          goods: goods,
          comments: goods.comments,
        })

        that.downloadImg(that.data.goods.photo_images[0], 1) //保存图片分享用
        that.downloadImg(that.data.goods.qrcode, 3)

        WxParse.wxParse('content', 'html', goods.content, that, 0);

        if (goods.spec_group) {
          that.setData({
            mulType: true
          })
        } else {
          that.setData({
            selectid: goods.products.product_id
          })
        }
        if (goods.video) {
          that.setData({
            imgsNum: goods.photo_images.length + 1
          })
        } else {
          that.setData({
            imgsNum: goods.photo_images.length
          })
        }
      } else {
        app.showToast({
          title: '网络错误',
          icon: 'warning'
        })
      }

    })

  },


  collageLoginInfo(id) {
    app.get('/api/collage/collageLoginInfo', {
      id: id
    }, (res) => {
      if (res.data.code == 200) {
        var data = res.data.data;
        this.setData({
          collect: data.collect,
          is_collage_leader: data.is_collage_leader,
          self_group_list: data.self_group_list,
          normal_group_list: data.normal_group_list,
          group_num: data.group_num,
          limit_num: data.limit_num
        })
        this.clearSetIntvalIds();
        if (data.normal_group_list && data.normal_group_list.length > 0) {
          this.getTimer(data.normal_group_list, 0)
        }
        if (data.self_group_list && data.self_group_list.length > 0) {
          this.getTimer(data.self_group_list, 1)
        }
      }
    })
  },
  //处理获取的规格
  getSpec(spec_group) {
    var that = this
    var spec = spec_group
    if (spec_group) {
      for (var i = 0; i < spec.length; i++) {
        spec[i].checked = 0
      }
      that.setData({
        spec: spec
      })
      that.getselText()
    }
  },

  //选择规格
  select_spec(e) {
    // console.log(e)
    var that = this
    var spec = this.data.spec
    var fidx = e.currentTarget.dataset.fidx
    var checkId = e.currentTarget.dataset.idx
    var selText = ''
    spec[fidx].checked = checkId
    that.setData({
      spec: spec
    })
    that.getselText()
  },

  //拼接文字
  getselText() {
    var that = this
    var spec = this.data.spec
    var products = this.data.goods.products
    var selText = ''
    for (var i = 0; i < spec.length; i++) {
      var itemCheckid = spec[i].checked
      selText = selText + spec[i].value[itemCheckid] + ','
    }
    selText = selText.slice(0, selText.length - 1)
    that.setData({
      selectSpec: selText,
      selectid: products[selText].id, //获取规格id
      selectImg: products[selText].image, //规格图片
      selectPrice: products[selText].real_price, //规格价格
    })
    if (this.data.buyType == 0) { //单买价
      that.setData({
        selectPrice: products[selText].real_price
      })
    } else { //拼团价
      that.setData({
        selectPrice: products[selText].collage_price
      })
    }
    // console.log(products[selText].id)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.closeConfirm();
    this.collageLoginInfo(this.data.id);
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    this.collageLoginInfo(this.data.id);
    wx.showLoading({
      title: '刷新中',
    })
    setTimeout(function() {
      wx.hideLoading();
      wx.stopPullDownRefresh();
    }, 1500)
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },

  //选择活动
  chooseAct() {
    var that = this
    that.setData({
      showAcitivity: true,
      showConfirm: true,
      popUp: true,
      playVideo: false,
    })
  },

  //数量加减
  changeNum(e) {
    var that = this
    var num = this.data.num
    var op = e.currentTarget.dataset.op
    if (op == 1) {
      num++
    } else {
      if (num == 1) {
        return
      } else {
        num--
      }
    }
    that.setData({
      num: num
    })
  },

  //查看大图
  previewImage() {
    var photos = this.data.goods.photo_images
    var index = this.data.pic_index - 2
    wx.previewImage({
      current: photos[index],
      urls: photos
    })
  },

  //播放、退出视频
  playVideo() {
    var that = this
    that.setData({
      playVideo: true
    })
  },

  stopPlay() {
    var that = this
    that.setData({
      playVideo: false
    })
  },
  //顶部tab显隐
  showTab(e) {
    // console.log(e)
    if (e.detail.scrollTop >= 100) {
      this.setData({
        showTab: true
      })
    } else {
      this.setData({
        showTab: false
      })
    }
  },

  //选择促销活动
  select_act(e) {
    this.setData({
      selectAct: e.currentTarget.dataset.act
    })
    this.closeConfirm()
  },

  //拼团
  buyGroup() {
    var goods_id = this.data.goods.goods_id
    var product_id = this.data.selectid
    var num = this.data.num
    var group_id = this.data.group_id
    var collage_id = this.data.id
    var paraStr = ''
    if (this.data.joingroup) { //参团
      paraStr = 'goods_id=' + goods_id + '&product_id=' + product_id + '&number=' + num + '&group_id=' + group_id;

      wx.navigateTo({
        url: '../pay/collage_pay/collage_pay?' + paraStr
      })

    } else { //发起
      var data = {
        goods_id: goods_id,
        product_id: product_id,
        number: num,
        collage_id: collage_id
      };
      var url = '';

      //判断团长身份开团还是普通用户身份开团
      if (this.data.leader_create_collage == 1 && this.data.is_collage_leader == 1) {
        url = '/api/collage/launchCreateCollage'
      } else {
        url = '/api/collage/launchJoinCollage'
      }
      app.post(url, {
        goods_id: goods_id,
        product_id: product_id,
        number: num,
        collage_id: collage_id
      }, function(res) {
        if (res.data.code == 200) {
          paraStr = 'goods_id=' + goods_id + '&product_id=' + product_id + '&number=' + num + '&group_id=' + res.data.data.group_id;

          wx.navigateTo({
            url: '../pay/collage_pay/collage_pay?' + paraStr
          })
        }
      })
    }

  },

  //单买
  buySingle() {
    //先添加到购物车
    app.post('/api/order/directOrder', {
      goods_id: this.data.goods.goods_id,
      product_id: this.data.selectid,
      number: this.data.num
    }, function(res) {
      if (res.data.code == 200) {
        wx.navigateTo({
          url: '../pay/pay',
        })
      }
      console.log(res)
    })
  },

  //联系客服
  callService() {
    wx.makePhoneCall({
      phoneNumber: app.globalData.phoneNumber
    })
  },

  //记录是单人还是立即团购
  buyOp(e) {
    var op = e.currentTarget.dataset.op
    this.stopPlay()
    this.showPop()
    this.setData({
      buyType: op
    })
    this.getSpec(this.data.goods.spec_group)
  },

  //确定
  submit() {
    this.closeConfirm()
    var buyType = this.data.buyType
    if (buyType == 0) {
      this.buySingle()
    } else {
      this.buyGroup()
    }
  },

  //收藏
  collect(e) {
    var that = this
    that.setData({
      collect: !this.data.collect
    })
    var del = this.data.collect;
    if (del == false) {
      that.setData({
        delet: 1
      })
    }
    if (del == true) {
      that.setData({
        delet: 0
      })
    }
    var delet = this.data.delet;
    var id = this.data.id;
    app.get('/api/goodscollection/collect', {
      type: 1,
      field_id: id,
      delete: delet,
    }, function(res) {
      if (res.data.code == 200) {
        wx.showToast({
          title: res.data.msg,
        })
      } else {
        wx.showToast({
          title: res.data.msg,
          icon: 'none'
        })
      }
    })
  },

  //计时器
  getTimer(contList, grouptype) {
    var that = this;
    var id = setInterval(function() {
      var list = contList
      for (var i = 0; i < list.length; i++) {
        list[i].lifecycle--
          if (list[i].lifecycle <= 0) {
            list.splice(i, 1)
          }
        else {
          list[i].timer = that.formatSeconds(list[i].lifecycle)
        }
      }
      if (grouptype == 0) {
        that.setData({
          normal_group_list: list,
        })
      } else {
        that.setData({
          self_group_list: list,
        })
      }
    }, 1000);
    var setIntvalIds = this.data.setIntvalIds
    this.data.setIntvalIds[id] = id;
    this.setData({
      setIntvalIds: setIntvalIds
    })
  },

  clearSetIntvalIds() {
    for (var i in this.data.setIntvalIds) {
      clearInterval(i);
    }
    this.setData({
      setIntvalIds: {}
    })
  },

  //格式化时间
  formatSeconds(value) {
    var secondTime = parseInt(value); // 秒
    var minuteTime = 0; // 分
    var hourTime = 0; // 小时
    if (secondTime > 60) {
      minuteTime = parseInt(secondTime / 60);
      secondTime = parseInt(secondTime % 60);
      if (minuteTime > 60) {
        hourTime = parseInt(minuteTime / 60);
        minuteTime = parseInt(minuteTime % 60);
      }
    }
    var result = "" + parseInt(secondTime) + "";
    if (secondTime < 10) {
      result = "" + '0' + parseInt(secondTime) + ""
    }
    if (minuteTime < 10) {
      result = "" + '0' + parseInt(minuteTime) + ":" + result;
    } else {
      result = "" + parseInt(minuteTime) + ":" + result;
    }
    if (hourTime < 10) {
      result = "" + '0' + parseInt(hourTime) + ":" + result;
    } else {
      result = "" + parseInt(hourTime) + ":" + result;
    }
    return result;
  },

  goping(e) { //参团
    var that = this
    this.getSpec(this.data.goods.spec_group)
    that.setData({
      joingroup: true,
      group_id: e.currentTarget.id,
      buyType: 1,
      showAcitivity: false
    })
    this.showPop()
  },
  startgroup() {
    var that = this;
    if (this.data.group_num >= this.data.limit_num) {
      return app.showToast({
        title: '请先完成未开成功的团',
        icon: 'warning'
      })
    }
    that.setData({
      startPing: true,
      popUp: true,
      popmask: true
    })
  },
  myjoin(e) {
    this.setData({
      leader_create_collage: 1
    })
    this.closeConfirm()
    this.buyOp(e)
  },


  inviteFri() {
    app.get('/api/Collage/launchCollage', {
      collage_id: this.data.id
    }, (res) => {
      if (res.data.code == 200) {
        var group_id = res.data.data;
        wx.navigateTo({
          url: '/pages/share/share?group_id=' + group_id,
        })
      } else {
        return app.showToast({
          title: res.data.msg,
          icon: 'warning'
        })
      }
    });

  },

  //分享按钮
  share() {
    var that = this
    that.setData({
      isShare: true,
      popmask: true
    })
  },

  //分享到朋友圈
  friendCicle() {
    var that = this
    that.drawcanvas()
    that.setData({
      friendship: true,
      isShare: false,
      popmask: true,
    })
  },


  //获取用户头像、昵称
  getUserInfo() {
    var that = this
    that.setData({
      avatarUrl: app.globalData.userInfo.avatar,
      nickName: app.globalData.userInfo.nickname,
    })
    that.downloadImg(that.data.avatarUrl, 2)
  },

  downloadImg(Url, imgtype) {
    console.log(Url)
    var that = this
    wx.downloadFile({
      url: Url,
      success(res) {
        if (imgtype == 1) {
          that.setData({
            canvasImg: res.tempFilePath
          })
        } else if (imgtype == 2) {
          that.setData({
            canvasAvatar: res.tempFilePath
          })

        } else {
          that.setData({
            canvasQr: res.tempFilePath
          })
        }
      }
    })
  },

  drawcanvas: function() {
    // wx.showLoading({
    //   title: '生成图片中',
    // })
    var that = this
    const ctx = wx.createCanvasContext('myCanvas')
    ctx.fillStyle = "rgb(203, 14, 14)";
    ctx.fillRect(0, 0, 350, 40);
    ctx.drawImage(that.data.canvasImg, 55, 110, 240, 240)
    ctx.setFontSize(16)
    ctx.fillStyle = '#fff'
    ctx.setTextAlign('center')
    ctx.fillText('我在美物上发现了一个宝贝，分享给你', 175, 27)
    var ava_width = 40; //头像宽度
    var ava_height = 40; //头像高度
    var ava_x = 20;
    var ava_y = 50;
    var goodsname = that.data.goods.name.slice(0, 10) + '...'
    ctx.save();
    ctx.beginPath();
    ctx.arc(ava_width / 2 + ava_x, ava_height / 2 + ava_y, ava_width / 2, 0, Math.PI * 2, false);
    ctx.clip();
    ctx.drawImage(that.data.canvasAvatar, ava_x, ava_y, ava_width, ava_height);
    ctx.restore();
    ctx.setFontSize(14)
    ctx.fillStyle = '#222'
    ctx.setTextAlign('center')
    ctx.fillText(that.data.nickName, 90, 78)
    ctx.setFontSize(16)
    ctx.fillStyle = '#222'
    ctx.setTextAlign('left')
    ctx.fillText(goodsname, 30, 390)
    ctx.fillStyle = '#CB0E0E'
    ctx.setFontSize(14)
    ctx.setTextAlign('left')
    ctx.fillText('￥' + that.data.goods.real_price, 30, 430)
    ctx.fillStyle = '#999'
    ctx.setFontSize(10)
    ctx.setTextAlign('left')
    ctx.fillText('￥' + that.data.goods.origin_price, 30, 445)
    ctx.fillStyle = '#999'
    ctx.setTextAlign('left')
    ctx.fillText('长按二维码，查看详情', 30, 460)
    if (this.data.canvasQr == undefined) {
      wx.showToast({
        title: '缺少二维码图片',
        icon: 'none'
      })
    } else {
      ctx.drawImage(that.data.canvasQr, 220, 370, 100, 100)
      ctx.draw(true, function() {
        wx.canvasToTempFilePath({
          fileType: 'jpg',
          canvasId: 'myCanvas',
          success: function(res) {
            wx.hideLoading()
            that.setData({
              canvaspath: res.tempFilePath
            })
          },
          fail() {
            wx.showToast({
              title: '画图失败',
            })
          }
        })
      })
    }
  },
  //保存图片到手机
  saveCanvas(filePath) {
    var that = this;
    wx.saveImageToPhotosAlbum({
      filePath: this.data.canvaspath,
      success: function(res) {
        that.closeConfirm()
        wx.showToast({
          title: '保存成功',
        })
      },
      fail: function(res) {
        wx.showToast({
          title: '保存失败',
          icon: 'none'
        })
        if (res.errMsg.indexOf('cancel') == -1) {
          wx.openSetting({
            success: (res) => {}
          })
        }
      }
    })
  },

  //保存按钮
  saveImg() {
    this.saveCanvas()
  },
})