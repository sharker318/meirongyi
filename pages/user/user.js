//获取应用实例
const app = getApp()
let url = app.globalData.url;
let imgurl = url + '/assets/mini_program_img/';
let imgUrl = '/images/user/';
import auth from '../../auth/auth.js'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgurl: imgurl,
    imgUrl: imgUrl,
    myCollect: 0,
    baseInfo: null,
    orders_classify: [{
      title: '待支付',
      id: 1,
      img: imgUrl + 'me_icon1@2x.png'
    },
    {
      title: '待发货',
      id: 3,
      img: imgUrl + 'me_icon2@2x.png'
    },
    {
      title: '待收货',
      id: 4,
      img: imgUrl + 'me_icon3@2x.png'
    },
    {
      title: '已完成',
      id: 5,
      img: imgUrl + 'me_icon4@2x.png'
    }
    ],
    incomeAmount: 0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  beinghead() {
    wx.navigateTo({
      url: 'go_grouphead/go_grouphead',
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.baseUserInfo()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },


  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (e) {
    // var that = this;
    // return {
    //   title: app.globalData.userInfo.nickName + '邀请你一起购百丽赞',
    //   path: 'pages/index/index?from_id=' + that.data.baseInfo.id,
    //   imageUrl: imgurl + 'me_invite_share.png',
    //   success(res) {
    //     //console.log(res); 
    //   },
    // }
    var userId = wx.getStorageSync('userId')
    return {
      title: '邀请好友',
      path: '/pages/index/index?userId=' + userId
    }
  },

  //跳转到订单页指定标签下
  toOrders(e) {
    console.log(e.currentTarget.dataset.id)
    wx.navigateTo({
      url: '../orders/orders?id=' + e.currentTarget.dataset.id,
    })

  },
  /**
   * 获取用户基本信息
   */
  baseUserInfo: function (callback) {
    app.showToast('open_user_center');
    app.get('/api/ucenter/baseInfo', null, res => {
      if (res.statusCode == 200) {
        console.log(res)
        this.setData({
          baseInfo: res.data.data
        })
      }
      app.hideToast();
      typeof callback == 'function' ? callback() : "";
    }, null, true)
  },
  /**
   * 下拉刷新
   */
  onPullDownRefresh: function () {
    this.baseUserInfo(function () {
      wx.stopPullDownRefresh()
    });
  }

})