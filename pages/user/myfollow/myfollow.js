const app = getApp();
let url = app.globalData.url;
let imgurl = url + '/assets/mini_program_img/';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgurl: imgurl,
    list: '',
    nodata: false,
    selsectAll: false,
    pageNum: 1,
    pageSize: 10
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.getlist()
  },

  getlist() {
    var that = this;
    app.get('/api/goodscollection/myCollectList', {
      page_num: this.data.pageNum,
      page_size: this.data.pageSize,
    }, function(res) {
      if (res.data.code == 200) {
        console.log(res)
        var list = res.data.data.list
        var oldlist = that.data.list
        if (list != '') {
          for (var i = 0; i < list.length; i++) {
            list[i].checked = false
          }
          if (oldlist == '') {
            that.setData({
              list: list,
            })
          } else {
            that.setData({
              list: oldlist.concat(list),
            })
          }
        } else {
          if (oldlist == '') {
            that.setData({
              nodata: true
            })
          } else {
            wx.showToast({
              title: '已没有更多了~',
              icon: 'none'
            })
          }
        }
      } else {
        wx.showToast({
          title: res.data.msg,
          icon: 'none'
        })
      }
    })
  },

  //显示删除操作
  deleOp() {
    this.setData({
      isEdit: !this.data.isEdit
    })
  },

  //更换选中状态
  checkState(e) {
    var list = this.data.list
    var index = e.target.id
    list[index].checked = !list[index].checked
    this.setData({
      list: list
    })
    console.log(list)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    var that = this;
    wx.showToast({
      title: '正在刷新',
      icon: 'loading',
      duration: 1500,
      success() {
        that.setData({
          pageNum: 1,
          list: ''
        })
        that.getlist();
        wx.stopPullDownRefresh()
      }
    })
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    var that = this
    that.setData({
      pageNum: this.data.pageNum + 1
    })
    that.getlist()
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  goodsdetail: function(event) {
    var op = event.currentTarget.dataset.op;
    var field_id = event.currentTarget.dataset.field_id;
    // console.log(goods_id);
    if (op == 0) {
      wx.navigateTo({
        url: '../../goods_detail/goods_detail?id=' + field_id
      })
    } else if (op == 1) {
      wx.navigateTo({
        url: '../../collage_detail/collage_detail?id=' + field_id
      })
    }

  },

  del: function() {
    var listq = this.data.list;
    var st = ''
    for (var i = 0; i < listq.length; i++) {
      if (listq[i].checked == true) {
        st = st + listq[i].id + ','
      }
    }
    st = st.slice(0, st.length - 1)

    console.log(st)
    var that = this
    app.get('/api/goodscollection/noFocus', {
      ids: st
    }, function(res) {
      console.log(res.data.code)
      if (res.data.code == 200) {
        wx.showToast({
          title: '删除成功',
        })
        that.setData({
          list: ''
        })
        that.getlist()
      } else {
        wx.showToast({
          title: res.data.msg,
          icon: 'none'
        })
      }
    })
  }
})