const app = getApp()
let url = app.globalData.url;
let imgurl = url + '/assets/mini_program_img/';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: '',
    noData: '',
    imgurl: imgurl,
    page_num: 10,
    page_size: 1
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.getList()
  },

  getList() {
    var that = this
    app.get('/api/collage/myCollageGroup', {
      page_num: this.data.page_num,
      page_size: this.data.page_size
    }, function(res) {
      if (res.data.code == 200) {
        if (res.data.data != null) {
          that.setData({
            noData: false,
            list: res.data.data
          })
          that.getTimer(res.data.data)
        } else {
          that.setData({
            noData: true,
          })
        }
      } else {
        wx.showToast({
          title: res.data.msg,
          icon: 'none'
        })
      }
      console.log(res)
    })
  },

  //计时器
  getTimer(contList) {
    var that = this
    var counter = setInterval(function() {
      var list = contList
      var timecount = 0
      for (var i = 0; i < list.length; i++) {
        list[i].lifecycle--
          if (list[i].lifecycle <= 0) {
            timecount++
            list[i].timer = '00:00:00'
            if (timecount == list.length) {
              clearInterval(counter)
            }
          }
        else {
          list[i].timer = that.formatSeconds(list[i].lifecycle)
        }
      }
      that.setData({
        list: list,
      })
    }, 1000)
  },


  //格式化时间
  formatSeconds(value) {
    var secondTime = parseInt(value); // 秒
    var minuteTime = 0; // 分
    var hourTime = 0; // 小时
    if (secondTime > 60) {
      minuteTime = parseInt(secondTime / 60);
      secondTime = parseInt(secondTime % 60);
      if (minuteTime > 60) {
        hourTime = parseInt(minuteTime / 60);
        minuteTime = parseInt(minuteTime % 60);
      }
    }
    var result = "" + parseInt(secondTime) + "";
    if (secondTime < 10) {
      result = "" + '0' + parseInt(secondTime) + ""
    }
    if (minuteTime < 10) {
      result = "" + '0' + parseInt(minuteTime) + ":" + result;
    } else {
      result = "" + parseInt(minuteTime) + ":" + result;
    }
    if (hourTime < 10) {
      result = "" + '0' + parseInt(hourTime) + ":" + result;
    } else {
      result = "" + parseInt(hourTime) + ":" + result;
    }

    return result;
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})