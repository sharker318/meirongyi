//获取应用实例
const app = getApp()
let url = app.globalData.url;
let imgurl = url + '/assets/mini_program_img/';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    nodata: false,
    imgurl: imgurl
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.getList();
  },

  getList: function() {
    wx.showLoading({
      title: '加载中',
    });
    var that = this;
    app.get('/api/Notification/getList', {}, function(res) {
      wx.hideLoading()
      console.log(res)
    
      if (res.data.code == 200) {
        var list = res.data.data
        if (list != '') {
          that.setData({
            notifyList: res.data.data,
          })
        } else {
          console.log('aa')
          that.setData({
            nodata: true,
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})