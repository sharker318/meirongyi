// pages/user/sales_volume/sales_volume.js
const app = getApp();
let url = app.globalData.url;
let imgurl = url + '/assets/mini_program_img/';
Page({

    /**
     * 页面的初始数据
     */
    data: {
      imgurl: imgurl,
      incomeList: []
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var that = this;
        //获取屏幕宽高
        wx.getSystemInfo({
            success: function (res) {
                // 高度,宽度 单位为px
                that.setData({
                    windowHeight:  res.windowHeight,
                    windowWidth:  res.windowWidth
                })
            }
        })

      this.getList();
    },

  //获取销售额记录
    getList: function () {
      var _this = this
      app.get('/api/finance/incomeLog', {
      }, function (res) {
        if (res.data.code == 200) {
          _this.setData({
            incomeList: res.data.data.list
          })
        }
      })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },
    //返回上一级
    fanhui: function () {
        wx.navigateBack({
            delta: 1
        })
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})