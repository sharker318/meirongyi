// pages/user/balance/balance.js
const app = getApp()
let url = app.globalData.url;
let imgurl = url + '/assets/mini_program_img/';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgurl: imgurl,
    balance: '',
    nodata: false,
    page_num: 1,
    page_size: 10,
    log: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  getlist() {
    var that = this;
    app.post('/api/finance/balanceLog', {
      page_num: this.data.page_num,
      page_size: this.data.page_size,
      type: 1,
    }, function(res) {
      console.log(res);
      wx.hideLoading()
      var list = that.data.log
      if (res.data.code == 200) {
        if (res.data.data.log) {
          if (list == '') {
            that.setData({
              balance: res.data.data.balance,
              log: res.data.data.log
            })
          } else {
            that.setData({
              balance: res.data.data.balance,
              log: list.concat(res.data.data.log)
            })
          }

        } else {
          that.setData({
            balance: res.data.data.balance,
            nodata: true
          })
        }
      }
    })
  },

  //去提现
  gotixian: function() {
    if (this.data.balance <= 0) {
      wx.showToast({
        title: '没有可提现余额哦~',
        icon: 'none'
      })
      return
    } else {
      wx.navigateTo({
        url: '/pages/user/put_forward/put_forward',
      })
    }

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    wx.showLoading({
      title: '加载中',
    })
    this.getlist()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    var that = this;
    wx.showToast({
      title: '正在刷新',
      icon: 'loading',
      duration: 1500,
      success() {
        that.setData({
          page_num: 1,
          log: '',
        })
        that.getlist();
        wx.stopPullDownRefresh()
      }
    })
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    wx.showLoading({
      title: '加载中',
    })
    var num = this.data.page_num
    this.setData({
      page_num: num++,
    })
    this.getlist()
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})