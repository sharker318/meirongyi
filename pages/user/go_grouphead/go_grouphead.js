// pages/go_grouphead/go_grouphead.js
const app = getApp();
let url = app.globalData.url;
let imgurl = url + '/assets/mini_program_img/';
Page({

    /**
     * 页面的初始数据
     */
    data: {
      imgurl: imgurl
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
      // app.showToast('open_grouphead');
      this.collageLeader();
    },



    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    go_group:function(){
      return wx.redirectTo({
          url: '/pages/collage/collage',
        });
    },
    //立即开团 
    upgrade(){
      var that = this;
      app.get('/api/ucenter/upgrade',{},(res) => {
        if(res.data.code == 200 ){
          app.showToast({
            title: res.data.msg,
            icon: 'success'
          })
          this.setData({
            status :2
          })
        }
        if(res.data.code == 400){
          app.showToast({
            title: res.data.msg,
            icon: 'warning'
          })
        }
      })
    },
  /**
   * 是否为可以成为团长,0不可以，1可以，2已是团长
   */
  collageLeader: function(){
    app.get('/api/ucenter/availableCollageLeader', null, (res) => {
      this.setData({
        status: res.data.data.status
      })
    } , null  ,true)
  }
})