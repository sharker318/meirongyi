// pages/user/bargain/bargain.js
const app = getApp()
let url = app.globalData.url;
let imgurl = url + '/assets/mini_program_img/';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: null
  },

  // 获取砍价的记录
  getlist(){
    var that = this
    app.post('/api/bargain/bargainsList','',function(res){
      // console.log(res)
      that.setData({
        list: res.data.data
      })
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    var that = this
    that.getlist()
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})