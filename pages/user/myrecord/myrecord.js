// pages/user/myrecord/myrecord.js
const app = getApp();
let url = app.globalData.url;
let imgurl = url + '/assets/mini_program_img/';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isEdit: false,
    selsectAll: false,
    nodata: false,
    list: '',
    imgurl: imgurl,
    pageNum: 1,
    pageSize: 10,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.getlist()
  },

  //获取记录列表
  getlist() {
    wx.showLoading({
      title: '加载中',
    })
    var that = this;
    app.get('/api/goodscollection/lookRecordList', {
      page_num: this.data.pageNum,
      page_size: this.data.pageSize,
    }, function(res) {
      wx.hideLoading()
      console.log(res)
      var list = res.data.data.list
      var oldlist = that.data.list
      if (res.data.code == 200) {
        if (list != '') {
          for (var i = 0; i < list.length; i++) {
            list[i].checked = false
          }
          if (oldlist == '') {
            that.setData({
              list: list,
              selsectAll: false,
            })
          } else {
            that.setData({
              list: oldlist.concat(list),
              selsectAll: false,
            })
          }
          console.log(that.data.list)
        } else {
          if (oldlist == '') {
            that.setData({
              nodata: true
            })
          } else {
            wx.showToast({
              title: '已没有更多记录~',
              icon: 'none'
            })
          }
        }
      }
    })
  },

  //清空记录
  clear: function() {
    var that = this
    wx.showModal({
      title: '提示信息',
      content: '确定要清空浏览记录吗？',
      confirmColor: '#CB0E0E',
      success(res) {
        if (res.confirm) {
          app.get('/api/goodscollection/clearAll', {}, function(res) {
            console.log(res)
            if (res.data.code == 200) {
              wx.showToast({
                title: '操作成功',
              })
              setTimeout(function() {
                that.setData({
                  list: ''
                })
                that.getlist()
              }, 300)
            } else {
              wx.showToast({
                title: res.data.msg,
              })
            }
          })
        }
      }
    })
  },

  //显示删除操作
  deleOp() {
    this.setData({
      isEdit: !this.data.isEdit
    })
  },

  //更换选中状态
  checkState(e) {
    var list = this.data.list
    var index = e.target.id
    list[index].checked = !list[index].checked
    this.setData({
      list: list
    })
    var Num = 0
    for (var i = 0; i < list.length; i++) {
      if (list[i].checked) {
        Num++
      }
    }
    if (Num == list.length) {
      this.setData({
        selsectAll: true
      })
    } else {
      this.setData({
        selsectAll: false
      })
    }
  },

  //全选
  selAll() {
    var that = this
    that.setData({
      selsectAll: !that.data.selsectAll
    })

    var list = that.data.list
    for (var i = 0; i < list.length; i++) {
      if (list[i].checked != that.data.selsectAll) {
        list[i].checked = that.data.selsectAll
      }
    }
    console.log(list)
    that.setData({
      list: list
    })

  },

  //删除
  del: function() {
    var listq = this.data.list;
    console.log(listq);
    var st = ''
    for (var i = 0; i < listq.length; i++) {
      if (listq[i].checked == true) {
        st = st + listq[i].id + ','
      }
    }
    st = st.slice(0, st.length - 1)

    console.log(st)
    var that = this
    app.get('/api/goodscollection/del', {
      ids: st
    }, function(res) {
      console.log(res.data.code)
      if (res.data.code == 200) {
        wx.showToast({
          title: '删除成功',
        })
        setTimeout(function() {
          that.setData({
            list: ''
          })
          that.getlist()
        }, 300)

      } else {
        wx.showToast({
          title: 'res.data.msg',
          icon: 'none'
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    var that = this;
    wx.showToast({
      title: '正在刷新',
      icon: 'loading',
      duration: 1500,
      success() {
        that.setData({
          pageNum: 1,
          list: ''
        })
        that.getlist();
        wx.stopPullDownRefresh()
      }
    })
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    var that = this
    that.setData({
      pageNum: this.data.pageNum + 1
    })
    that.getlist()
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },

  goodsDetail: function(event) {
    var goods_id = event.currentTarget.dataset.id;
    console.log(goods_id)
    wx.navigateTo({
      url: '../../goods_detail/goods_detail?id=' + goods_id
    })
  }
})