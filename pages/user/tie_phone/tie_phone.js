const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    phone: '',
    code: '',
    time: 60,
    downshow: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  //获取手机号
  getphone(e) {
    this.setData({
      phone: e.detail.value
    })
  },

  //获取验证码
  getCode() {
    var that = this
    var myreg = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1})|(17[0-9]{1}))+\d{8})$/;
    if (!that.data.phone){
      wx.showToast({
        title: '请填写手机号',
        icon: 'loading',
        duration: 1000
      })
    } else if (!myreg.test(that.data.phone)) {
      wx.showToast({
        title: '请填写正确手机号',
        icon: 'loading',
        duration: 1000
      })
    }else{
      app.get('/api/sms/send', {
        mobile: that.data.phone,
        event: 'bindmobile'
      }, function (res) {
        if(res.data.code === 200){
          console.log(that)
          that.downtime()
        }else{
          wx.showToast({
            title: res.data.msg,
            icon: 'none',
            duration: 1000
          })
        }
      })
    }
  },

  //获取输入的验证码
  textCode(e) {
    this.setData({
      code: e.detail.value
    })
  },

  //提交
  submit() {
    app.post('/api/ucenter/bindPhone', {
      mobile: this.data.phone
    }, function(res) {
      if (res.data.code == 200){
        wx.showToast({
          title: res.data.msg,
          icon: 'success',
          duration: 1000
        })
        setTimeout(function(){
          wx.switchTab({
            url: '/pages/user/user'
          })
        },1200)
      }else{
        wx.showToast({
          title: res.data.msg,
          icon: 'loading',
          duration: 1000
        })
      }
    })
  },

  // 验证短信验证码
  chechsmg(){
    var that = this
    app.get('/api/sms/check', {
      mobile: that.data.phone,
      event: 'bindmobile',
      captcha: that.data.code
    }, function (res) {
      if (res.data.code == 200){
        that.submit()
        wx.showToast({
          title: res.data.msg,
          icon: 'success',
          duration: 1000
        })
      }else{
        wx.showToast({
          title: res.data.msg,
          icon: 'loading',
          duration: 1000
        })
      }
    })
  },

  // 倒计时
  downtime(){
    var that = this
    this.setData({
      downshow: !that.data.downshow
    })
    let count = this.data.time
    var timer = setInterval(function () {
      count--;
      if (count >= 1) {
        that.setData({
          time: count
        });
      } else {
        that.setData({
          downshow: !that.data.downshow,
          time: 60
        });
        clearInterval(timer);
      }
    }, 1000);
  }
})