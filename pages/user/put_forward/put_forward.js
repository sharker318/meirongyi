const app = getApp();
let url = app.globalData.url;
let imgurl = url + '/assets/mini_program_img/';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgurl: imgurl,
    selected: true,
    selected1: false,
    success: false,
    yue: '20.6',
    ye: '',
    verify_day:5
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.verifyDate();
    var that = this;
    app.post('/api/finance/balanceLog', {
      type: 1
    }, function(res) {
      console.log(res);
      if (res.data.code == 200) {
        that.setData({
          yue: res.data.data.balance,
        })
      }
    })
  },

  //顶部切换
  selected: function(e) {
    this.setData({
      selected1: false,
      selected: true
    })
  },
  selected1: function(e) {
    this.setData({
      selected: false,
      selected1: true
    })
  },
  //跳转提现记录
  go_present_record: function() {
    wx.navigateTo({
      url: '/pages/user/present_record/present_record',
    })
  },
  //点击选择所有余额
  all_yue: function() {
    var that = this;
    that.setData({
      ye: this.data.yue
    })
  },

  formSubmit: function(e) {
    var selectbank = this.data.selected;
    if (selectbank) { //银行卡
      this.setData({
        balance_type: 3
      })
    } else { //支付宝
      this.setData({
        balance_type: 2
      })
    }
    var form_data = e.detail.value;
    form_data.withdraw_way = this.data.balance_type;
    app.post('/api/finance/applyWithdraw', form_data, (res) => {
      if (res.data.code == 200) {
        this.setData({
          success: true
        })

        setTimeout(function() {
          wx.navigateBack({
            delta: 1
          })
        }, 2000)
      } else {
        wx.showToast({
          title: res.data.msg,
          icon: 'none'
        })
      }
    })
  },

  //提现成功弹
  gotixian_ok: function(e) {
    this.setData({
      success: true
    })
    setTimeout(function() {
      wx.navigateBack({
        delta: 1
      })
    }, 2000)
  },
  /**
   * 审核时间
   */
  verifyDate(){
    var date = new Date();//获取当前时间
    var verify_date = app.globalData.verify_day ? app.globalData.verify_day : this.data.verify_day;
    date.setDate(date.getDate() + verify_date);//设置天数 +5 天
    var month = date.getMonth();
    var day = date.getDate(); 
    this.setData({
      verify_date : {
        month:month,
        day:day,
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})