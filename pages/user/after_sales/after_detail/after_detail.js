// pages/user/after_sales/after_detail/after_detail.js
const app = getApp()
let url = app.globalData.url;
let imgurl = url + '/assets/mini_program_img/';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    writeNo: false,
    deliver_index: 0,
    companyArray: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this
    this.getData(options.id);
  },

  //获取售后详情
  getData: function(id) {
    wx.showLoading({
      title: '加载中',
    })
    var that = this;
    app.get('/api/aftersale/getDetail', {
      aftersale_id: id
    }, function(res) {
      console.log(res)
      wx.hideLoading();
      if (res.data.code == 200) {
        that.setData({
          detailInfo: res.data.data
        })
      } else {
        wx.showToast({
          title: '网络错误',
          image: '../../images/warning.png'
        })
      }
    })
  },

  //获取物流详情
  getDeliveryCompany: function() {
    var that = this;
    app.get('/api/freight/getDeliveryCompany', null, function(res) {
      if (res.data.code == 200) {
        that.setData({
          companyArray: res.data.data
        })
      }
    })
  },


  operate: function(e) {
    var id = this.data.detailInfo.id;
    var op = e.currentTarget.dataset.op;
    if (op == 'cancelApply') {
      this.cancelApply(id)
    } else if (op == 'writeDeliverNo') {
      this.writeDeliverNo(id)
    } else if (op == 'getDeliveryInfo') {
      wx.navigateTo({
        url: '../../../logistics/logistics?type=1&id=' + id
      })
    }
  },

  //取消售后申请
  cancelApply(id) {
    var that = this;
    wx.showModal({
      title: '确定取消吗',
      content: '',
      success(res) {
        if (res.confirm) {
          that.confirmCancel(id)
        }
      }
    })
  },

  confirmCancel(id) {
    var that = this;
    wx.showLoading({
      title: '提交中',
    })
    app.get('/api/aftersale/cancelApply', {
      aftersale_id: id
    }, function(res) {
      wx.hideLoading();
      console.log(res);
      if (res.data.code == 200) {
        wx.showToast({
          title: '提交成功',
          icon: 'success',
          duration: 1000,
          success: function() {
            that.setData({
              writeNo: false,
              deliver_index: 0
            });
            setTimeout(function() {
              wx.navigateBack()
            }, 1000);
          }
        })
      } else {
        wx.showToast({
          title: '提交失败',
          icon: 'none',
          duration: 1000,
        })
        that.setData({
          writeNo: false,
          deliver_index: 0
        });
      }
    })
  },

  //打开物流填写弹窗
  writeDeliverNo(id) {
    var that = this
    that.setData({
      writeNo: true,
      aftersaleId: id
    });
    this.getDeliveryCompany();
  },

  //获取物流单号
  getDeliveryNo: function(e) {
    // console.log(e.detail.value);
    this.setData({
      delivery_no: e.detail.value
    });
  },

  //提交物流单号
  submitWriteNo: function() {
    wx.showLoading({
      title: '加载中',
    })
    var that = this;
    var data = {
      aftersale_id: that.data.aftersaleId,
      delivery_company_id: that.data.companyArray[that.data.deliver_index].id,
      delivery_no: that.data.delivery_no
    };
    app.post('/api/aftersale/writeDeliverNo', data, function(res) {
      wx.hideLoading();
      if (res.data.code == 200) {
        wx.showToast({
          title: '提交成功',
          icon: 'success',
          duration: 1000,
          success: function() {
            that.setData({
              writeNo: false,
              deliver_index: 0
            });
            that.getData(that.data.aftersaleId);
          }
        })
      } else {
        wx.showToast({
          title: '提交失败',
          icon: 'none',
          duration: 1000,
        })
        that.setData({
          writeNo: false,
          deliver_index: 0
        });
      }
    })

  },

  //取消物流单号的填写
  cancelWriteNo: function() {
    var that = this
    that.setData({
      writeNo: false,
      deliver_index: 0
    });
    console.log('取消');
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})