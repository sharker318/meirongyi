//获取应用实例
const app = getApp()
let url = app.globalData.url;
let imgurl = url + '/assets/mini_program_img/';
Page({
  /**
   * 页面的初始数据
   */
  data: {
    currentTab: 1,
    writeNo: false,
    imgurl: imgurl,
    noData: false,
    tabs: [{
        id: 1,
        title: '申请售后'
      },
      {
        id: 2,
        title: '处理中'
      },
      {
        id: 3,
        title: '申请记录'
      }

    ],


    deliver_index: 0,
    companyArray: []
  },

  bindPickerChange: function(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      deliver_index: e.detail.value
    })
  },

  // tab栏的切换
  tabChange: function(e) {
    this.setData({
      currentTab: e.currentTarget.dataset.id,
      list: ''
    })
    this.getlist()
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

    this.getDeliveryCompany();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  getlist() {
    wx.showLoading({
      title: '加载中',
    })
    var that = this
    var tab = this.data.currentTab
    var url = ''
    if (tab == 1) {
      url = '/api/aftersale/getEnableApply'
    } else if (tab == 2) {
      url = '/api/aftersale/getApplying'
    } else {
      url = '/api/aftersale/getList'
    }
    var that = this
    app.get(url, {}, function(res) {
      wx.hideLoading()
      console.log(res)
      if (res.data.code == 200) {
        if (res.data.data != '') {
          that.setData({
            list: res.data.data,
            noData: false
          })
        } else {
          that.setData({
            noData: true
          })
        }

      } else {
        wx.showToast({
          title: res.data.msg,
          icon: 'none'
        })
      }
    })
  },

  getDeliveryCompany: function() {
    var that = this;
    app.get('/api/freight/getDeliveryCompany', null, function(res) {
      if (res.data.code == 200) {
        that.setData({
          companyArray: res.data.data
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.setData({
      list:''
    })
    
    this.getlist();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },

  //提交申请
  apply(e) {
    console.log(e)
    var op = e.currentTarget.dataset.op
    var id = e.currentTarget.id
    if (op == 'apply') {
      wx.navigateTo({
        url: 'apply/apply?id=' + e.currentTarget.id,
      })
    } else if (op == 'cancelApply') {

      this.cancelApply(id)
    } else if (op == 'writeDeliverNo') {
      this.writeDeliverNo(id)
    } else if (op == 'getDeliveryInfo') {
      wx.navigateTo({
        url: '../../logistics/logistics?type=1&id=' + id
      })
    }
  },

  //取消售后申请
  cancelApply(id) {
    var that = this;
    wx.showModal({
      title: '确定取消吗',
      content: '',
      success(res) {
        if (res.confirm) {
          that.confirmCancel(id)
        }
      }
    })
  },
  confirmCancel(id) {
    var that = this;
    app.get('/api/aftersale/cancelApply', {
      aftersale_id: id
    }, function(res) {
      if (res.data.code == 200) {
        wx.showToast({
          title: '取消成功',
          icon: 'success',
          duration: 1000,
          success: function() {
            that.setData({
              writeNo: false,
              deliver_index: 0,
              list: []
            });
            that.getlist();
          }
        })
      } else {
        wx.showToast({
          title: '取消失败',
          icon: 'none',
          duration: 1000,
        })
        that.setData({
          writeNo: false,
          deliver_index: 0
        });
      }
    })
  },

  //打开物流填写弹窗
  writeDeliverNo(id) {
    var that = this
    that.setData({
      writeNo: true,
      aftersaleId: id
    })
  },

  //获取物流单号
  getDeliveryNo: function(e) {
    // console.log(e.detail.value);
    this.setData({
      delivery_no: e.detail.value
    });
  },

  //提交物流单号
  submitWriteNo: function() {
    wx.showLoading({
      title: '加载中',
    })
    var that = this
    var data = {
      aftersale_id: that.data.aftersaleId,
      delivery_company_id: that.data.companyArray[that.data.deliver_index].id,
      delivery_no: that.data.delivery_no
    };
    app.post('/api/aftersale/writeDeliverNo', data, function(res) {
      wx.hideLoading();
      if (res.data.code == 200) {
        wx.showToast({
          title: '提交成功',
          icon: 'success',
          duration: 1000,
          success: function() {
            that.setData({
              writeNo: false,
              deliver_index: 0
            });
            that.onLoad();
          }
        })
      } else {
        wx.showToast({
          title: '提交失败',
          icon: 'none',
          duration: 1000,
        })
        that.setData({
          writeNo: false,
          deliver_index: 0
        });
      }
    })

  },

  //取消物流单号的填写
  cancelWriteNo: function() {
    var that = this
    that.setData({
      writeNo: false,
      deliver_index: 0
    });
    console.log('取消');
  }
})