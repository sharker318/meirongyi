import COOKIE from '../../../../utils/cookie.js';
const app = getApp()
let url = app.globalData.url;
let imgurl = url + '/assets/mini_program_img/';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    id: '',
    content: '',
    imgurl: imgurl,
    imgpath: [],
    currentTab: 1,
    applyimg: '',
    apptype: [{
        id: 1,
        name: '换货',
        img_sel: imgurl + 'aftersale_exchange_sel.png',
        img_nor: imgurl + 'aftersale_exchange_nor.png'
      },
      {
        id: 2,
        name: '退货',
        img_sel: imgurl + 'aftersale_return_sel.png',
        img_nor: imgurl + 'aftersale_return_nol.png'
      },
      {
        id: 3,
        name: '维修',
        img_sel: imgurl + 'aftersale_service_sel.png',
        img_nor: imgurl + 'aftersale_service_nor.png'
      },
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.setData({
      id: options.id
    })
    this.getinfo(options.id)
  },

  switchtab(e) {
    var that = this
    that.setData({
      currentTab: e.currentTarget.id
    })
    console.log(e.currentTarget.id)
  },

  getinfo(id) {
    var that = this
    wx.showLoading({
      title: '加载中',
    })
    app.get('/api/aftersale/getDataToApply', {
      order_goods_id: id
    }, function(res) {
      wx.hideLoading()
      if (res.data.code == 200) {
        that.setData({
          info: res.data.data
        })
      }
      console.log(res)
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  //选择图片
  choose_img: function(e) {
    var that = this
    var imgs = this.data.imgpath
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function(res) {
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
        var tempFilePaths = res.tempFilePaths
        imgs.push(tempFilePaths)
        that.setData({
          imgpath: imgs
        })

        that.upimg(tempFilePaths[0])
      }
    })
  },
  //上传图片
  upimg: function(uppath) {
    var cookie = COOKIE.getUserCookie();
    var cookestr = ''
    if (cookie) {
      var c = 0
      for (var i in cookie) {
        if (c !== 0) {
          cookestr += " "
        }
        cookestr += i + '=' + cookie[i];
        c++;
      }
    }
    var that = this
    var list = this.data.applyimg
    wx.uploadFile({
      url: url + '/api/comment/uploadImage',
      filePath: uppath,
      name: 'file', //文件对应的参数名字(key)
      header: {
        'Cookie': cookestr
      },
      success: function(res) {
        var result = JSON.parse(res.data);
        console.log(result)
        if (result.code == 200) {
          list = list + ',' + (result.data.url)
          // list = list.substr(1, list.length)
          that.setData({
            applyimg: list
          })
          console.log(list)
        }
      }
    })

  },



  //获取文本
  textdown: function(e) {
    var that = this
    that.setData({
      content: e.detail.value
    })
  },

  //提交评价
  submit: function() {
    if (this.data.content == '') {
      wx.showToast({
        title: '请输入申请原因',
        icon: 'none'
      })
      return
    }
    var that = this

    var data = {
      order_goods_id: this.data.id,
      number: 1,
      type: this.data.currentTab,
      reason: this.data.content,
      images: this.data.applyimg.substr(1, this.data.applyimg.length)
    }
    wx.showLoading({
      title: '正在提交',
    })
    app.post('/api/aftersale/submitApply', data, function(res) {
      console.log(res)
      wx.hideLoading()
      if (res.data.code == 200) {
        wx.showToast({
          title: '已提交，等待审核！',
          icon: 'success',
          duration: 1000,
          mask: true,
        })
        setTimeout(function() {
          wx.navigateBack()
        }, 1000)
      } else {
        wx.showToast({
          title: res.data.msg,
          duration: 1000,
          mask: true,
        })
      }
    })
  }
})