// pages/user/about/about.js
const WxParse = require('../../../utils/wxParse/wxParse.js')
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var app = getApp();
    var tha = this;
    var da = app.globalData.rule.content;
    WxParse.wxParse('content', 'html', da, tha, 0);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  // 申请方法
  applay() {
    app.post('/api/ucenter/upgrade', '', function (res) {
      if (res.data.code === 200) {
        wx.showToast({
          title: res.data.msg,
          icon: 'success',
          duration: 1200
        })
        setTimeout(function () {
          wx.switchTab({
            url: '/pages/user/user',
          })
        }, 1200)
      } else {
        wx.showToast({
          title: res.data.msg,
          icon: 'loading',
          duration: '1000'
        })
      }
    })
  }
})