const app = getApp()
let url = app.globalData.url;
let imgurl = url + '/assets/mini_program_img/';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    currentTab: 1,
    list: '',
    imgurl: imgurl,
    nodata: false,
    tabs: [{
        id: 1,
        title: '可使用'
      },
      {
        id: 2,
        title: '已失效'
      }
    ],
  },

  // tab栏的切换
  tabChange: function(e) {
    this.setData({
      currentTab: e.currentTarget.dataset.id,
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.getlist()
  },

  //获取banner
  getlist() {
    var that = this
    app.get('/api/coupon/myList', {}, function(res) {
      console.log(res)
      if (res.data.code == 200) {
        that.setData({
          list: res.data.data
        })
        if (res.data.data == '') {
          that.setData({
            nodata: true
          })
        }
      } else {
        wx.showToast({
          title: res.data.msg,
          icon: 'none'
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  usecoupon(){
    wx.switchTab({
      url: '/pages/index/index',
    })
  }

})