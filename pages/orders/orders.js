//获取应用实例
const app = getApp()
let url = app.globalData.url;
let imgurl = url + '/assets/mini_program_img/';
Page({
  data: {
    currentTab: 0,
    imgurl: imgurl,
    pageNum: 1,
    pageSize: 5,
    list: '',
    nodata: false,
    tabs: [{
        id: 0,
        title: '全部'
      },
      {
        id: 1,
        title: '待付款'
      },
      // {
      //   id: 2,
      //   title: '待成团'
      // },
      {
        id: 3,
        title: '待发货'
      },
      {
        id: 4,
        title: '待收货'
      },
      // {
      //   id: 5,
      //   title: '待评价'
      // }
    ],
  },
  // tab栏的切换
  tabChange: function(e) {
    this.setData({
      currentTab: e.currentTarget.dataset.id,
      pageNum: 1,
      list: ''
    })
    this.getOrderList()
  },

  toDetail() {
    wx.navigateTo({
      url: '../goods_detail/goods_detail',
    })
  },
  onLoad: function(options) {
    // console.log(options.id)
    if (options.id != undefined) {
      this.setData({
        currentTab: options.id
      })
    }
    // this.getOrderList()
  },

  onShow: function() {
    this.setData({
      list: [],
      pageNum:1
    })
    this.getOrderList()
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
   
    var that = this;
    wx.showToast({
      title: '正在刷新',
      icon: 'loading',
      duration: 1500,
      success() {
        that.setData({
          pageNum: 1,
          list: ''
        })
        that.getOrderList();
        wx.stopPullDownRefresh()
      }
    })
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    var that = this
    that.setData({
      pageNum: this.data.pageNum + 1
    })
    that.getOrderList()
  },
  //获取订单列表
  getOrderList() {
    app.showToast({
      identifier:'loading_order',
      icon:'loading',
      title: '加载中',
    })
    var that = this
    var orderlist = this.data.list
    var para = {
      page_num: this.data.pageNum,
      page_size: this.data.pageSize,
      step: this.data.currentTab
    }
    that.setData({
      nodata: false
    })
    app.get('/api/order/getList', para, function(res) {
     
      if (res.data.code == 200) {
        if (orderlist.length == 0) {
          that.setData({
            list: res.data.data
          })
        } else {
          that.setData({
            list: orderlist.concat(res.data.data)
          })
        }
      }
      if (that.data.list == '') {
        that.setData({
          nodata: true
        })
      }
    })
  },

  //订单操作
  orderOp(e) {
    var that = this
    var op = e.currentTarget.dataset.op
    var idx = e.currentTarget.dataset.idx;
    var id = e.currentTarget.id
    if (op == 'cancel' || op == 'delete' || op == 'remove') {
      wx.showModal({
        title: '确认' + e.currentTarget.dataset.tip + '吗？',
        confirmColor: '#CB0E0E',
        success(res) {
          if (res.confirm) {
            that.sendOP(op, id ,idx)
          }
        }
      })
    } else if (op == 'pay') {
      that.doPay(id, idx)
    } else if (op == 'evaluate') {
      return wx.navigateTo({
        url: 'write_evaluate/write_evaluate?orderid=' + id,
      })
    } else if (op == 'delivery') {
      return wx.navigateTo({
        url: '../logistics/logistics?type=0&id=' + id
      })
    }
    else if(op == 'call_cancel'){
      return wx.makePhoneCall({
        phoneNumber: app.globalData.phoneNumber,
      })
    }else {
      that.sendOP(op, id, idx)
    }
  },
  //操作请求
  sendOP(op, id , idx) {
    var that = this
    app.get('/api/order/operate', {
      order_id: id,
      operate: op
    }, function(res) {
        if (op == 'again'){
            if(res.statusCode == 200){
              return wx.switchTab({
                url: '/pages/cart/cart',
              });
            }else{
              return wx.showModal({
                content: res.data.msg,
                showCancel:false
              })
            }
        }
      if (res.data.code == 200) {
        if (op == 'reset') {
          that.doPay(id ,idx)
        }else {
          app.showToast({
            title: res.data.msg,
            icon: 'success'
          })
          var order_list = that.data.list;
           if (op == 'delete' || op == 'remove') {
              order_list.splice(idx,1);
            }else{
             order_list[idx] = res.data.data;
            }
          that.setData({
            list: order_list
          })

        }
      }
    })
  },

  //支付
  doPay: function(id,idx) {
    wx.showToast({
      title: '订单处理中',
      icon: 'loading'
    })
    var that = this
    app.post('/api/miniprogram.payment/unifiedOrder', {
      order_id: id
    }, function(res) {
      if (res.statusCode == 200) {
        var payObj = res.data.data;
        payObj.success = function() {
          //todo 这里写支付成功后的逻辑操作
          wx.showToast({
            title: '支付成功',
            icon: 'none'
          })
          that.renewRow(id, idx)
        }
        payObj.fail = function() {
          //todo 这里写取消支付的逻辑
          wx.showToast({
            title: '您取消了支付',
            icon: 'none'
          })
          that.renewRow(id, idx)
        }
        wx.requestPayment(payObj)
      }

    })
  },
  /**
   * 更新orderList中的条记录
   * @param int id 订单id
   * @param int idx orderList中的key下标
   */
  renewRow:function(id, idx){
     app.get('/api/order/renewRow', {order_id: id},(res) => {
       if (res.statusCode == 200){
         var order_list = this.data.list;
         order_list[idx] = res.data.data;
         this.setData({
           list:order_list
         })
       }
     })
  }
})