//获取应用实例
const app = getApp()
let url = app.globalData.url;
let imgurl = url + '/assets/mini_program_img/';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderInfo: '',
    imgurl: imgurl,
    address: '',
    good: '',
    id: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    console.log(options.order_id)
    this.setData({
      id: options.order_id
    })
    this.getOrder(options.order_id)
  },

  //获取订单详情
  getOrder(id) {
    wx.showLoading({
      title: '加载中',
    })
    var that = this
    app.get('/api/order/getRow', {
      order_id: id
    }, function(res) {
      wx.hideLoading()
      console.log(res)
      if (res.data.code == 200) {
        that.setData({
          orderInfo: res.data.data,
          address: res.data.data.consignee_info,
          goods: res.data.data.goods_info
        })
      }

    })
  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },

  //订单操作
  orderOp(e) {
    var that = this
    var op = e.currentTarget.dataset.op
    if (op == 'cancel' || op == 'delete' || op == 'remove') {
      wx.showModal({
        title: '确认' + e.currentTarget.dataset.tip + '吗？',
        confirmColor: '#CB0E0E',
        success(res) {
          if (res.confirm) {
            that.sendOP(op)
          }
        }
      })
    } else if (op == 'pay') {
      that.doPay()
    } else if (op == 'evaluate') {
      wx.navigateTo({
        url: '../write_evaluate/write_evaluate?orderid=' + this.data.id,
      })
    } else if (op == 'delivery') {
      wx.navigateTo({
        url: '../../logistics/logistics?type=0&id=' + this.data.id
      })
    } else if (op == 'call_cancel') {
      that.callService()
    } else {
      that.sendOP(op)
    }
  },




  //操作请求
  sendOP(op) {
    var that = this
    app.get('/api/order/operate', {
      order_id: this.data.id,
      operate: op
    }, function(res) {
      console.log(res)
      if (res.data.code == 200) {
        if (op == 'reset') {
          that.doPay()
        } else {
          wx.showToast({
            title: res.data.msg,
          })
          setTimeout(function() {
            wx.navigateBack()
          }, 1000)
        }
      }
    })
  },



  //支付
  doPay: function() {
    wx.showToast({
      title: '订单处理中',
      icon: 'loading'
    })
    var that = this
    app.post('/api/miniprogram.payment/unifiedOrder', {
      order_id: this.data.orderInfo.id
    }, function(res) {
      if (res.statusCode == 200) {
        var payObj = res.data.data;
        payObj.success = function() {
          //todo 这里写支付成功后的逻辑操作
          wx.showToast({
            title: '支付成功',
            icon: 'none'
          })
          setTimeout(function() {
            wx.navigateBack()
          }, 500)
        }
        payObj.fail = function() {
          //todo 这里写取消支付的逻辑
          wx.showToast({
            title: '您取消了支付',
            icon: 'none'
          })
          setTimeout(function() {
            that.getOrder(that.data.id)
          }, 500)
        }
        wx.requestPayment(payObj)
      }

    })
  },

  //联系卖家
  callService() {
    wx.makePhoneCall({
      phoneNumber: app.globalData.phoneNumber
    })
  },

})