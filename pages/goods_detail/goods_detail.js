const WxParse = require('../../utils/wxParse/wxParse.js') //获取应用实例
const app = getApp()
let url = app.globalData.url;
let imgurl = url + '/assets/mini_program_img/';
Page({
  data: {
    cg_id: '',
    currentTab: 'v1',
    imgurl: imgurl,
    id: '',
    cut_id: '',
    goods: '',
    collect: false,
    num: 1,
    delet: 0,
    imgsNum: 0,
    selectid: '',
    buyType: 0,
    isSelect: false,
    popUp: false,
    showConfirm: false,
    showAcitivity: false,
    friendship: false,
    popmask: false,
    showTab: false,
    pic_index: 1,
    playVideo: false,
    spec: '',
    isShare: false,
    selectSpec: '',
    selectAct: '',
    selectImg: '',
    selectPrice: '',
    mulType: false,
    comments: '',
    tabs: [{
        id: 'v1',
        title: '商品'
      },
      // {
      //   id: 'v2',
      //   title: '评价'
      // },
      {
        id: 'v3',
        title: '详情'
      }
    ],
  },

  // tab栏的切换
  tabChange: function(e) {
    var tabId = e.currentTarget.dataset.id
    this.setData({
      currentTab: tabId,
    })
  },

  //图片索引切换
  showId(e) {
    var picIdx = e.detail.current + 1
    this.setData({
      pic_index: picIdx
    })
  },

  showPop() {
    this.setData({
      showConfirm: true,
      popUp: true,
      popmask: true
    })
  },


  // 创建砍价
  createKan(){
    var that = this
    var data = {
      cutprice_id: that.data.cut_id,
      cg_id: this.data.cg_id
    }
    app.post('/api/bargain/createActivity',data,function(res){
      console.log(res)
      if(res.data.code == 200){
        wx.redirectTo({
          url: '/pages/bargain/bargain?cut_id=' + that.data.cg_id + '&&goods_id=' + that.data.id + '&&cu_id=' + res.data.data.cu_id,
        })
      }
    })
  },


  //关闭弹窗
  closeConfirm() {
    this.setData({
      showConfirm: false,
      showAcitivity: false,
      popUp: false,
      isShare: false,
      popmask: false,
      friendship: false,
      id: ''
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    console.log(options)
    this.setData({
      id: options.id
    })
    this.getUserInfo()
    this.setData({
      id: options.id
    })
    wx.showLoading({
      title: '加载中',
    })
    //获取商品数据
    this.getDetail(options.id)
  },

  getDetail(id) {
    var that = this
    app.get('/api/goods/detail', {
      goods_id: id
    }, function(res) {
      wx.hideLoading()
      console.log(res)
      if (res.data.code == 200) {
        var goods = res.data.data
        WxParse.wxParse('content', 'html', goods.content, that, 0);
        that.setData({
          goods: goods,
          comments: goods.comments,
          selectAct: goods.activity[0].name,
          collect: goods.collect,
          cg_id: goods.default_product.cg_id
        })
        if (goods.activity.length!==0){
          for (let i = 0; i < goods.activity.length;i++){
            if (goods.activity[i].label == '砍价'){
              that.setData({
                cut_id: goods.activity[i].id
              })
            }
          }
        }
        that.downloadImg(that.data.goods.photo_images[0], 1) //保存图片分享用
        that.downloadImg(that.data.goods.qrcode, 3)
        if (goods.spec_group && goods.spec_group.length !== 0) {
          that.getSpec(goods.spec_group)
          that.setData({
            mulType: true
          })
        } else {
          that.setData({
            selectid: goods.products.id
          })
        }
        if (goods.video) {
          that.setData({
            imgsNum: goods.photo_images.length + 1
          })
        } else {
          that.setData({
            imgsNum: goods.photo_images.length
          })
        }
      } else if (res.data.code == 400) {
        app.showToast({
          title: res.data.msg,
          icon: 'warning'
        })
      } else {
        app.showToast({
          title: '网络错误',
          icon: 'warning'
        })
      }
    })
  },

  //处理获取的规格
  getSpec(spec_group) {
    var that = this
    var spec = spec_group
    for (var i = 0; i < spec.length; i++) {
      spec[i].checked = 0
    }
    that.setData({
      spec: spec
    })

    that.getselText()
  },

  //选择规格
  select_spec(e) {
    console.log(e)
    var that = this
    var spec = this.data.spec
    var fidx = e.currentTarget.dataset.fidx
    var checkId = e.currentTarget.dataset.idx
    var selText = ''
    spec[fidx].checked = checkId
    that.setData({
      spec: spec
    })
    that.getselText()

    // 处理cg_id
    var list = that.data.goods.product_list
    for (let i = 0; i < list.length;i++){
      if (list[i].spec_group == ('规格版本:' + that.data.selectSpec) ){
        that.setData({
          cg_id: list[i].cg_id
        })
      }
    }
  },

  //拼接文字
  getselText() {
    var that = this
    var spec = this.data.spec
    var products = this.data.goods.products
    var selText = ''
    for (var i = 0; i < spec.length; i++) {
      var itemCheckid = spec[i].checked
      selText = selText + spec[i].value[itemCheckid] + ','
    }
    selText = selText.slice(0, selText.length - 1)
    that.setData({
      selectSpec: selText,
      selectid: products[selText].id, //获取规格id
      selectImg: products[selText].image, //规格图片
      selectPrice: products[selText].real_price, //规格价格
    })
    console.log(products[selText].id)
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    var that = this;
    var shareObj = {
      title: that.data.goods.name,        // 默认是小程序的名称(可以写slogan等)
      path: '/pages/goods_detail/goods_detail?id=' + that.data.goods.id,
      // 默认是当前页面，必须是以‘/’开头的完整路径
      imgUrl: '',
      //自定义图片路径，可以是本地文件路径、代码包文件路径或者网络图片路径，支持        PNG及JPG，不传入 imageUrl 则使用默认截图。显示图片长宽比是 5:4
      success: function (res) {
        if (res.errMsg == 'shareAppMessage:ok') {
        }
      },
      fail: function () {
        if (res.errMsg == 'shareAppMessage:fail cancel') {
          // 用户取消转发
        } else if (res.errMsg == 'shareAppMessage:fail') {
          // 转发失败，其中 detail message 为详细失败信息
        }
      },
      complete: function () {
        // 转发结束之后的回调（转发成不成功都会执行）
      }
    };
    if (options.from == 'button') {
      var eData = options.target.dataset;
      console.log(eData.name);     // shareBtn // 此处可以修改 shareObj 中的内容
      shareObj.path = '/pages/btnname/btnname?btn_name=' + eData.name;　　　　
    }
　　// 返回shareObj return shareObj;
  },

  //选择活动
  chooseAct() {
    var that = this
    that.setData({
      showAcitivity: true,
      showConfirm: true,
      popUp: true
    })
  },

  //数量加减
  changeNum(e) {
    var that = this
    var num = this.data.num
    var op = e.currentTarget.dataset.op
    if (op == 1) {
      num++
    } else {
      if (num == 1) {
        return
      } else {
        num--
      }
    }
    that.setData({
      num: num
    })
  },

  //查看大图
  previewImage() {
    var photos = this.data.goods.photo_images
    var index = this.data.pic_index - 2
    wx.previewImage({
      current: photos[index],
      urls: photos
    })
  },

  //播放、退出视频
  playVideo() {
    var that = this
    that.setData({
      playVideo: true
    })
  },

  stopPlay() {
    var that = this
    that.setData({
      playVideo: false
    })
  },
  //顶部tab显隐
  showTab(e) {
    // console.log(e)
    if (e.detail.scrollTop >= 100) {
      this.setData({
        showTab: true
      })
    } else {
      this.setData({
        showTab: false
      })
    }
  },

  //选择促销活动
  select_act(e) {
    this.setData({
      selectAct: e.currentTarget.dataset.act
    })
    this.closeConfirm()
  },

  //添加到购物车
  addCart() {
    // this.showPop()
    app.get('/api/cart/addCart', {
      goods_id: this.data.goods.id,
      product_id: this.data.selectid,
      num: this.data.num
    }, function(res) {
      if (res.data.code == 200) {
        wx.showToast({
          title: res.data.msg,
        })
      }
      console.log(res)
    })
  },

  //立即购买
  buyNow() {
    //先添加到购物车
    app.post('/api/order/directOrder', {
      goods_id: this.data.goods.id,
      product_id: this.data.selectid,
      number: this.data.num
    }, function(res) {
      console.log(res)
      if (res.data.code == 200) {
        wx.navigateTo({
          url: '../pay/pay',
        })
      } else {
        wx.showToast({
          title: '下单出错',
          icon: 'none'
        })
      }
    })

  },

  //联系客服
  callService() {
    wx.makePhoneCall({
      phoneNumber: app.globalData.phoneNumber
    })
  },

  //记录是加入还是立即购买
  buyOp(e) {
    var op = e.currentTarget.dataset.op
    this.stopPlay()
    if (this.data.isSelect && op != 0) {
      if (op == 1) {
        this.addCart()
      } else if (op == 2) {
        this.buyNow()
      }
    } else {
      this.showPop()
      this.setData({
        buyType: op
      })
    }

    console.log(op)

  },

  //确定
  submit() {
    this.closeConfirm()
    var buyType = this.data.buyType
    if (buyType == 0) {
      this.setData({
        isSelect: true
      })
      return
    } else if (buyType == 1) {
      this.addCart()
    } else {
      this.buyNow()
    }
  },

  //收藏
  collect(e) {

    var that = this
    that.setData({
      collect: !this.data.collect
    })
    var del = this.data.collect;

    if (del == false) {
      console.log(321)
      that.setData({
        delet: 1
      })
    }
    if (del == true) {
      that.setData({
        delet: 0
      })
    }
    var delet = this.data.delet;
    var id = this.data.id;

    app.get('/api/goodscollection/collect', {
      type: 0,
      field_id: id,
      delete: delet,
    }, function(res) {

      if (res.data.code == 200) {
        // wx.showToast({
        //   title: res.data.msg,
        // })
      }

    })
  },

  //分享按钮
  share() {
    var that = this
    that.setData({
      isShare: true,
      popmask: true,
    })

  },

  //分享到朋友圈
  friendCicle() {
    var that = this
    that.drawcanvas()
    that.setData({
      friendship: true,
      isShare: false,
      popmask: true,
    })
  },


  //获取用户头像、昵称
  getUserInfo() {
    var that = this
    that.setData({
      avatarUrl: app.globalData.userInfo.avatar,
      nickName: app.globalData.userInfo.nickname,
    })
    that.downloadImg(that.data.avatarUrl, 2)
  },

  downloadImg(Url, imgtype) {
    var that = this
    wx.downloadFile({
      url: Url,
      success(res) {
        if (imgtype == 1) {
          that.setData({
            canvasImg: res.tempFilePath
          })
        } else if (imgtype == 2) {
          that.setData({
            canvasAvatar: res.tempFilePath
          })
        } else {
          that.setData({
            canvasQr: res.tempFilePath
          })
        }
        console.log(res)
      }
    })
  },

  drawcanvas: function() {
    // wx.showLoading({
    //   title: '生成图片中',
    // })
    var _this = this
    const ctx = wx.createCanvasContext('myCanvas')
    ctx.fillStyle = "rgb(203, 14, 14)";
    ctx.fillRect(0, 0, 350, 40);
    ctx.drawImage(this.data.canvasImg, 55, 110, 240, 240)
    ctx.setFontSize(16)
    ctx.fillStyle = '#fff'
    ctx.setTextAlign('center')
    ctx.fillText('我在美物上发现了一个宝贝，分享给你', 175, 27)
    var ava_width = 40; //头像宽度
    var ava_height = 40; //头像高度
    var ava_x = 20;
    var ava_y = 50;
    var goodsname = this.data.goods.name.slice(0, 10) + '...'
    ctx.save();
    ctx.beginPath();
    ctx.arc(ava_width / 2 + ava_x, ava_height / 2 + ava_y, ava_width / 2, 0, Math.PI * 2, false);
    ctx.clip();
    ctx.drawImage(this.data.canvasAvatar, ava_x, ava_y, ava_width, ava_height);
    ctx.restore();
    ctx.setFontSize(14)
    ctx.fillStyle = '#222'
    ctx.setTextAlign('center')
    ctx.fillText(this.data.nickName, 90, 78)
    ctx.setFontSize(16)
    ctx.fillStyle = '#222'
    ctx.setTextAlign('left')
    ctx.fillText(goodsname, 30, 390)
    ctx.fillStyle = '#CB0E0E'
    ctx.setFontSize(14)
    ctx.setTextAlign('left')
    ctx.fillText('￥' + this.data.goods.real_price, 30, 430)
    ctx.fillStyle = '#999'
    ctx.setFontSize(10)
    ctx.setTextAlign('left')
    ctx.fillText('￥' + this.data.goods.origin_price, 30, 445)
    ctx.fillStyle = '#999'
    ctx.setTextAlign('left')
    ctx.fillText('长按二维码，查看详情', 30, 460)
    if (this.data.canvasQr == undefined) {
      wx.showToast({
        title: '缺少二维码图片',
        icon: 'none'
      })
    } else {
      ctx.drawImage(this.data.canvasQr, 220, 370, 100, 100)
      ctx.draw(true, function() {
        wx.canvasToTempFilePath({
          fileType: 'jpg',
          canvasId: 'myCanvas',
          success: function(res) {
            wx.hideLoading()
            _this.setData({
              canvaspath: res.tempFilePath
            })
          }
        })
      })
    }
  },
  //保存图片到手机
  saveCanvas() {
    var that = this;
    wx.saveImageToPhotosAlbum({
      filePath: this.data.canvaspath,
      success: function(res) {
        that.closeConfirm()
        wx.showToast({
          title: '保存成功',
        })
      },
      fail: function(res) {
        wx.showToast({
          title: '保存失败',
          icon: 'none'
        })
        if (res.errMsg.indexOf('cancel') == -1) {
          wx.openSetting({
            success: (res) => {}
          })
        }
      }
    })
  },

  //保存按钮
  saveImg() {
    this.saveCanvas()
  }

})