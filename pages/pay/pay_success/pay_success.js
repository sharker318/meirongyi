const app = getApp()
let url = app.globalData.url;
let imgurl = url + '/assets/mini_program_img/';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgurl: imgurl,
    showShare:false,
    group_id:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (options.from == 'pay'){
      this.setData({
        showShare: false,
      })
    }
    if (options.from == 'colpay') {
      this.setData({
        showShare: true,
      })
    }
    if (options.group_id){
      this.setData({
        group_id: options.group_id
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  inviteFri() {
    app.get('/api/Collage/launchCollage', {
      group_id: this.data.group_id
    }, (res) => {
      if (res.data.code == 200) {
        var group_id = res.data.data;
        wx.navigateTo({
          url: '/pages/joincollage/joincollage?group_id=' + group_id,
        })
      } else {
        return app.showToast({
          title: res.data.msg,
          icon: 'warning'
        })
      }
    });

  },
})