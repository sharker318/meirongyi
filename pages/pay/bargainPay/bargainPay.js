//获取应用实例
const app = getApp()
let url = app.globalData.url;
let imgurl = url + '/assets/mini_program_img/';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgurl: imgurl,
    orderInfo: '',
    address_id: '',
    invoice_id: '',
    cu_id: '',
    goSelect: false,
    showConfirm: false,
    isSelcoupon: false,
    isInvoice: false,
    remark: '',
    order_id: '', //刚生成的订单
    couponList: '',
    couponid: '',
    old_order_id: '', //老订单id,再来一单的入口
    cupon_text: '无可用优惠'
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    wx.showLoading({
      title: '加载中',
    })

    app.post('/api/order/purchase', {}, function (res) {

      console.log(res.data.data)
      console.log(res.data.data.address)
      wx.hideLoading()
      if (res.data.code == 200) {
        var couponList = res.data.data.coupon_list
        for (var i in couponList) {
          if (res.data.data.coupon.id == couponList[i].id) {
            // couponList[i].cupon_status = '取消使用'
            couponList[i].cupon_status = ''
          } else {
            // couponList[i].cupon_status = '立即使用'
            couponList[i].cupon_status = ''
          }
        }
        that.setData({
          orderInfo: res.data.data,
          cu_id: res.data.data.coupon.id,
          couponList: res.data.data.coupon_list
        })
        if (res.data.data.address != '') {
          that.setData({
            address_id: res.data.data.address.id,
          })
        }
      }
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },


  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    console.log(this.data.address_id)
    if (this.data.goSelect) {
      //获取默认值
      this.getNewOrder()
    } else {
      //获取选择后信息

    }
  },

  getNewOrder() {
    var that = this
    wx.showLoading({
      title: '加载中',
    })

    var para = {
      address_id: this.data.address_id,
      invoice_id: this.data.invoice_id,
      cu_id: this.data.cu_id
    }
    app.post('/api/order/cartOrder', para, function (res) {
      wx.hideLoading()
      if (res.data.code == 200) {
        that.setData({
          orderInfo: res.data.data
        })
        console.log(that.data.couponList)
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  //弹出选择优惠券
  seleCoupon() {
    this.setData({
      showConfirm: true,
      isSelcoupon: true
    })
  },

  usecoupon(e) {
    var that = this
    var selectd = false
    var couponList = that.data.couponList
    if (e._relatedInfo.anchorTargetText == '取消使用') {
      selectd = true
    }
    for (var i in couponList) {
      if (couponList[i].id == e.currentTarget.id && couponList[i].cupon_status == '立即使用') {
        // couponList[i].cupon_status = '取消使用'
        couponList[i].cupon_status = ''
      } else {
        // couponList[i].cupon_status = '立即使用'
        couponList[i].cupon_status = ''
      }
    }
    if (selectd) {
      that.setData({
        cu_id: '',
        cupon_text: '未使用优惠券',
      })
    } else {
      that.setData({
        cu_id: e.currentTarget.id,
      })
    }
    that.setData({
      couponList: couponList
    })
    that.getNewOrder()
    that.closeConfirm()
  },
  //弹出选择发票
  seleInvo() {
    this.setData({
      showConfirm: true,
      isInvoice: true
    })
  },
  //关闭弹窗
  closeConfirm() {
    this.setData({
      showConfirm: false,
      isSelcoupon: false,
      isInvoice: false
    })
  },

  pay() {
    if (this.data.address_id == '') {
      wx.showToast({
        title: '请选择一个收货地址',
        icon: 'none'
      })
      return
    }
    if (this.data.order_id > 0) { //第二次后点击付款
      return this.doPay(this.data.order_id)
    }
    wx.showLoading({
      title: '加载中',
    })
    var para = {
      address_id: this.data.address_id,
      invoice_id: this.data.invoice_id,
      cu_id: this.data.cu_id,
      remark: this.data.remark
    }
    var that = this;

    app.post('/api/order/generateOrder', para, function (res) {
      wx.hideLoading()
      var order_id = res.data.data ? res.data.data.order_id : '';
      if (!order_id) {
        return wx.showToast({
          title: '下单失败！',
          icon: 'none'
        })
      }
      that.setData({
        order_id: order_id
      })
      that.doPay(order_id);
    })
  },

  doPay: function (order_id) {
    app.post('/api/miniprogram.payment/unifiedOrder', {
      order_id: order_id
    }, function (res) {
      if (res.statusCode == 200) {
        var payObj = res.data.data;
        payObj.success = function () {
          //todo 这里写支付成功后的逻辑操作
          setTimeout(function () {
            wx.redirectTo({
              url: 'pay_success/pay_success?from=pay',
            })
          }, 300)
        }
        payObj.fail = function () {
          //todo 这里写取消支付的逻辑
          wx.showToast({
            title: '您取消了支付',
            icon: 'none'
          })
        }
        wx.requestPayment(payObj)
      }

    })
  }
})