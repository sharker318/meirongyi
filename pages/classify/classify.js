//获取应用实例
const app = getApp()
let url = app.globalData.url;
let imgurl = url + '/assets/mini_program_img/';
import auth from '../../auth/auth.js'
Page({
  data: {
    currentTab: 0,
    imgurl: imgurl,
    category: '',
    goods: '',
    showPic: false,
    pageNum: 1,
    pageSize: 8,
    nodata: false,
    tag: '',
    backTopValue: false  
  },
  // tab栏的切换
  tabChange: function(e) {
    this.setData({
      currentTab: e.currentTarget.dataset.idx,
      tag: e.currentTarget.id,
      goods: '',
      pageNum: 1
    })
    this.getGoods(e.currentTarget.id)
  },
  // 跳转到商品详情
  toDetail(e) {
    wx.navigateTo({
      url: '../goods_detail/goods_detail?id=' + e.currentTarget.id,
    })
  },
  //跳转到搜索
  toSearch() {
    wx.navigateTo({
      url: '../search/search',
    })
  },

  onReachBottom: function() {
    var that = this
    that.setData({
      pageNum: this.data.pageNum + 1
    })
    that.getGoods(this.data.tag)
  },

  onLoad(options) {
    auth()
    this.getCategory();
  },
  //获取商品分类
  getCategory() {
    var that = this
    app.get('/api/category/getCategory', {}, function(res) {
      console.log(res)
      if (res.data.code == 200) {
        that.setData({
          category: res.data.data,
          tag: res.data.data[0].id
        })
        // var tabindex = app.globalData.activeTab
        // if (tabindex == 0)
        that.getGoods(res.data.data[0].id)
      } else {
        wx.showToast({
          title: res.data.msg,
        })
      }
    })
  },
  //根据分类获取商品
  getGoods(categoryId) {
    wx.showLoading({
      title: '加载中',
    })
    var that = this
    that.setData({
      nodata: false
    })
    var list = this.data.goods
    app.get('/api/category/getGoodsByCategory', {
      cat_id: categoryId,
      page_num: this.data.pageNum,
      page_size: this.data.pageSize,
    }, function(res) {
      wx.hideLoading()
      if (res.data.code == 200) {
        if (list.length == 0) {
          that.setData({
            goods: res.data.data.list
          })

        } else if (list.length != 0 && res.data.data.list.length == 0) {
          wx.showToast({
            title: '没有更多了。。。',
            icon:'none'
          })
        } else {
          that.setData({
            goods: list.concat(res.data.data.list)
          })
        }

      } else {
        wx.showToast({
          title: res.data.msg,
        })
      }
      if (that.data.goods == '') {
        that.setData({
          nodata: true
        })
      }
    })
  },


  //图片懒加载
  showPicture: function() {
    let that = this
    let show = that.data.showPic
    let animation = wx.createAnimation({
      duration: 300,
      timingFunction: 'ease-in',
      delay: 0
    })
    show = true
    animation.opacity(1).step()
    that.setData({
      showPic: show,
      animationData: animation.export()
    })
  },

  onPullDownRefresh: function() {

    var that = this;
    wx.showToast({
      title: '正在刷新',
      icon: 'loading',
      duration: 1500,
      success() {
        that.setData({
          pageNum: 1,
          goods: ''
        })
        that.getGoods(that.data.tag);
        wx.stopPullDownRefresh()
      }
    })
  },

  // 滚动到顶部
  backTop: function () {
    // 控制滚动
    wx.pageScrollTo({
      scrollTop: 0
    })
  },

  // 监听滚动条坐标
  onPageScroll: function (e) {
    //console.log(e)
    var that = this
    var scrollTop = e.scrollTop
    var backTopValue = scrollTop > 500 ? true : false
    that.setData({
      backTopValue: backTopValue
    })
  },

})