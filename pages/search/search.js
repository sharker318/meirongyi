const app = getApp()
let url = app.globalData.url;
let imgurl = url + '/assets/mini_program_img/';
Page({

  data: {
    goods: '',
    imgurl: imgurl,
    showPic: false,
    noData: false,
    searchCache: [],
    showCache: true,
    showCancel: false,
    hotsearch: '',
    pageNum: 1,
    pageSize: 10,
    list: ''
  },

  onLoad: function(options) {
    this.getHotsearch()
    this.getCache()
  },

  //获取热门搜索
  getHotsearch() {
    var that = this
    app.get('/api/index/getHotSearch', {}, function(res) {
      console.log(res)
      if (res.data.code == 200) {
        that.setData({
          hotsearch: res.data.data
        })
      }
    })
  },

  //获取搜索历史
  getCache() {
    var search = wx.getStorageSync('searchCache')
    if (search) {
      this.setData({
        searchCache: search
      })
    } else {
      this.setData({
        searchCache: []
      })
    }
  },
  //清除搜索历史
  clearSearch() {
    wx.removeStorageSync('searchCache')
    this.getCache()
  },

  //输入框搜索
  getSearch(e) {
    var keyword = e.detail.value
    var oldkeyword = this.data.keyword
    this.setData({
      keyword: keyword
    })
    if (oldkeyword != 'keyword') {

    }
    this.sendSearch()
  },
  searing() {
    this.setData({
      showCancel: true
    })
  },
  //点击输入历史搜索
  getItemsearch(e) {
    var keyword = e.currentTarget.dataset.keyword
    this.setData({
      keyword: keyword
    })
    this.sendSearch()
  },

  //搜索请求
  sendSearch() {
    var that = this
    that.putCache(this.data.keyword) //写入缓存
    wx.showLoading({
      title: '搜索中',
    })
    app.get('/api/index/searchGoods', {
      keywords: this.data.keyword,
      page_num: this.data.pageNum,
      page_size: this.data.pageSize
    }, function(res) {
      wx.hideLoading()
      console.log(res)
      var data = res.data.data
      var oldlist = that.data.list
      if (res.data.code == 200 && data.list.length != 0) {
        if (oldlist == '') {
          that.setData({
            noData: false,
            goods: data,
            list: data.list,
            showCache: false,
            showCancel: false
          })
        } else {
          that.setData({
            noData: false,
            goods: data,
            list: oldlist.concat(data.list),
            showCache: false,
            showCancel: false
          })
        }

      } else if (res.data.code == 200 && data.list.length == 0) {
        if (oldlist == '') {
          that.setData({
            noData: true,
            goods: '',
            showCache: false
          })
        } else {
          wx.showToast({
            title: '已没有更多了~',
            icon: 'none'
          })
        }

      } else {
        wx.showToast({
          title: res.data.msg,
        })
      }
    })
  },

  //搜索记录存入缓存
  putCache(keyword) {
    var search = this.data.searchCache
    search.unshift(keyword)
    var temp = []; //一个新的临时数组去重
    for (var i = 0; i < search.length; i++) {
      if (temp.indexOf(search[i]) == -1) {
        temp.push(search[i]);
      }
    }
    wx.setStorage({
      key: "searchCache",
      data: temp
    })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

    var that = this;
    wx.showToast({
      title: '正在刷新',
      icon: 'loading',
      duration: 1500,
      success() {
        that.setData({
          pageNum: 1,
          goods: '',
          list: ''
        })
        that.sendSearch();
        wx.stopPullDownRefresh()
      }
    })
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    var that = this
    that.setData({
      pageNum: this.data.pageNum + 1
    })
    that.sendSearch()
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  //跳转到详情页
  toDetail(e) {
    wx.navigateTo({
      url: '../goods_detail/goods_detail?id=' + e.currentTarget.id,
    })
  },

  //图片懒加载
  showPicture: function() {
    let that = this
    let show = that.data.showPic
    let animation = wx.createAnimation({
      duration: 300,
      timingFunction: 'ease-in',
      delay: 0
    })
    show = true
    animation.opacity(1).step()
    that.setData({
      showPic: show,
      animationData: animation.export()
    })
  },

  //取消搜索
  cancel() {
    this.setData({
      showCache: false,
      noData: false,
    })
  },
  //点击输入框显示搜索历史
  isSearch() {
    this.setData({
      showCache: true,
      noData: false,
    })
    this.getCache()
  }
})