//获取应用实例
const app = getApp()

Page({
  data: {
    province: '',
    cities: '',
    area: '',
    provinceId: '',
    citiesId: '',
    areaId: '',
    selectArea: '',
    region: ['北京市', '北京市', '东城区'],
    showpop: false,
    isEdit: false,
    addInfo: '',
    fromPay: false,

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    console.log(options)
    if (options.id) {
      wx.setNavigationBarTitle({
        title: '编辑收货地址',
      })
      this.setData({
        isEdit: true
      })
      this.getAddinfo(options.id)
    }
    if (options.fromPay) {
      this.setData({
        fromPay: true
      })
    }

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  //地区发生改变时
  bindRegionChange: function(e) {
    this.setData({
      region: e.detail.value
    })
  },


  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },

  //提交
  formSubmit(e) {
    var formdata = e.detail.value
    var isDefault = 0
    var phoneReg = /^1[3-578]\d{9}$/
    if (e.detail.value.isDefault || this.data.fromPay) {
      isDefault = 1
    }
    var para = {
      name: formdata.name,
      phone: formdata.phone,
      province: this.data.region[0],
      city: this.data.region[1],
      area: this.data.region[2],
      address: formdata.address,
      is_default: isDefault
    }
    if (this.data.isEdit) {
      para.id = this.data.addInfo.id
    }

    if (formdata.name == '' || formdata.phone == '' || formdata.address == '') {
      wx.showToast({
        title: '请填写完整信息',
        icon: 'none'
      })
      return
    } else if (!phoneReg.test(formdata.phone)) {
      wx.showToast({
        title: '请填写正确的手机号码',
        icon: 'none'
      })
      return
    } else {
      var that = this
      app.get('/api/address/addOrEditAddress', para, function(res) {
        console.log(res)
        if (res.data.code == 200) {
          wx.showToast({
            title: '保存成功！',
          })
          e.detail.value = ''

          if (that.data.fromPay) {

            var pages = getCurrentPages();
            // var currPage = pages[pages.length - 1]; //当前页面
            var prevPage = pages[pages.length - 2]; //上一个页面
            prevPage.setData({
              address_id: res.data.data.id,
              goSelect: true
            })

          }
          setTimeout(function() {
            wx.navigateBack()
          }, 1000)
        } else {
          return wx.showToast({
            title: res.data.msg,
            icon: 'none'
          })
        }
      })
    }
  },

  //弹出地址选择
  showPop() {
    this.setData({
      showpop: true
    })
  },

  //关闭地址选择
  closePop() {
    this.setData({
      showpop: false
    })
  },


  //编辑地址，获取原有信息
  getAddinfo(id) {
    var that = this
    wx.showLoading({
      title: '加载中',
    })
    app.get('/api/address/getAddressItem', {
      address_id: id
    }, function(res) {
      wx.hideLoading()
      console.log(res)
      var data = res.data.data
      that.setData({
        addInfo: data,
        region: [data.province_text, data.city_text, data.area_text]
      })
    })
  },

  //删除地址
  deleAdd() {
    var that = this
    var id = this.data.addInfo.id
    wx.showModal({
      title: '确认删除吗？',
      confirmColor:'#CB0E0E',
      success(res) {
        if (res.confirm) {
          app.get('/api/address/delAddress', {
            id: id
          }, function(res) {
            console.log(res)
            if (res.data.code == 200) {
              console.log('aaa')
              wx.showToast({
                title: '删除成功！',
              })
              setTimeout(function() {
                wx.navigateBack()
              }, 1000)
            }
          })
        }
      }
    })
  }
})