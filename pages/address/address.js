//获取应用实例
const app = getApp()
let url = app.globalData.url;
let imgurl = url + '/assets/mini_program_img/';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    addList: '',
    isSelect: 0,
    imgurl: imgurl
  },

  add_address() {
    wx.navigateTo({
      url: 'new_address/new_address',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    console.log(options)
    if (options.select) {
      this.setData({
        isSelect: options.select
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.getAddList()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },

  getAddList() {
    var that = this
    wx.showLoading({
      title: '加载中',
    })
    app.get('/api/address/getAddressList', {}, function(res) {
      wx.hideLoading()
      console.log(res)
      if (res.data.code == 200) {
        that.setData({
          addList: res.data.data
        })
      }
    })
  },

  selectAdd(e){
    console.log(e)
    if(this.data.isSelect == 1){
      var select_add = e.currentTarget.id
      var pages = getCurrentPages();
      // var currPage = pages[pages.length - 1]; //当前页面
      var prevPage = pages[pages.length - 2]; //上一个页面
      prevPage.setData({
        address_id: select_add,
        goSelect:true
      })
      wx.navigateBack();
    }
  }
})