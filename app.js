//app.js
import util from 'utils/util.js';
import auth from 'auth/auth.js';
import http from 'utils/http.js';
import tips from 'utils/tips.js';
App({
  onLaunch: function(res) {
    this.getAddData() //获取地址数据
    // this.globalData.url = http.baseUrl;
    this.globalData.url = http.portUrl;
    this.globalData.portUrl = http.portUrl;
    if (res.query.from_id >0){
      //如果是分享进来
      this.globalData.registerUrl = '/api/miniprogram/user/register?from_id=' + res.query.from_id ;
    }
  },

  //获取地址数据
  getAddData() {
    var is_area = 0;
    if (!wx.getStorageSync('AddData')) {
      is_area = 1;
    }
    this.get('/api/common/init', {
      area: is_area
    }, (res) => {

      if (res.data.code == 200) {
        if (res.data.data.area_tree) {
          wx.setStorage({
            key: "AddData",
            data: res.data.data.area_tree
          })
        }
        this.globalData.about = res.data.data.about;
        this.globalData.rule = res.data.data.rule;
        this.globalData.tips_list = res.data.data.tips_list;
        this.globalData.phoneNumber = res.data.data.phoneNumber;
        this.globalData.verify_date = res.data.data.verify_date;
      }
    })
  },

  globalData: {
    menuid: 0,
    userInfo: null,
    activeTab: 0,
    portUrl: '',
    url: "",
    settingData: {}, //order_limit
    dbUserInfo: null, //数据库中的用户信息,
    about: null,
    phoneNumber:'',
    loginUrl:'/api/miniprogram.user/login', //小程序登录api
    registerUrl: '/api/miniprogram.user/register' //注册api
  },
  loginCallbacks: [], //登录过程中要调用的方法集
  loginIn: false, //是否正在登录
  // util: util,
  /**
   * http请求
   * @param methd   string        方式 post|get
   * @param url     string        后台地址，可以带域名也可不带域名
   * @param data    object|string 请求数据
   * @paran success function      成功回调
   * @param error   function      失败回调
   * @param is_login int|bool     是否需要在登录前提下去请求
   */
  http(methd, url, data, success, error, is_login) {
    var hook = function(res){
       //如果授权失败,尝试去登录再来重试请求
        if(res.statusCode == 401){
            auth(function () {
              http.request(methd, url, data, success, error);
            })
            return ;
        }
        typeof success == 'function' ? success(res) : '' ;
    }
    if (is_login) {
      auth(function() {
        http.request(methd, url, data, hook, error);
      })
    } else {
      http.request(methd, url, data, hook, error);
    }
  },
  /**
   * http post请求
   * @param url     string        后台地址，可以带域名也可不带域名
   * @param data    object|string 请求数据
   * @paran success function      成功回调
   * @param error   function      失败回调
   * @param is_login int|bool     是否需要在登录前提下去请求
   */
  post(url, data, success, error, is_login) {
    return this.http('post', url, data, success, error, is_login);
  },
  /**
   * http get请求
   * @param url     string        后台地址，可以带域名也可不带域名
   * @param data    object|string 请求数据
   * @paran success function      成功回调
   * @param error   function      失败回调
   * @param is_login int|bool     是否需要在登录前提下去请求
   */
  get(url, data, success, error, is_login) {
    return this.http('get', url, data, success, error, is_login);
  },
  /**
   * 弹窗提示
   * @param obj wx.showToast的参数
   * 在obj内部增加identifier 标识符（自动补充标题）; icon:warning 警告类型
   * @return void
   */
  showToast(obj){
    tips.showToast( obj);
  },
  hideToast(obj){
    tips.hideToast(obj);
  }
})