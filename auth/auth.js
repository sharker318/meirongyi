import util from '../utils/util';
import http from '../utils/http.js';
import cookie from '../utils/cookie.js';
export default function auth(callback) {
  var app = getApp();
  var cook = cookie.getUserCookie();
  if (cook && cook.keeplogined && app.globalData.userInfo) {
    return typeof callback === 'function' ? callback() : '';
  }
  app.loginCallbacks.push(callback); //要回调的函数集
  var pageList = getCurrentPages();
  var pageObj = pageList[pageList.length - 1];
  login(function() {
    getUserInfo(callback)
  }, function() {
    pageObj.setData({
      ismask: true
    });
    pageObj.bindGetUserInfo = bindGetUserInfo;
  });
}


/**
 * 第一次去登录
 */
function getUserInfo() {
  var app = getApp();
  wx.getUserInfo({
    success: res => {
      // 可以将 res 发送给后台解码出 unionId
      app.globalData.userInfo = res.userInfo
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
      // 所以此处加入 callback 以防止这种情况
      if (app.userInfoReadyCallback) {
        app.userInfoReadyCallback(res)
      }
      sendQuery(res, function(ret) {
        app.globalData.userInfo = ret.data.data;
        for (var i in app.loginCallbacks) {
          var Callback = app.loginCallbacks[i]
          typeof Callback == 'function' ? Callback() : ''
        }
        app.loginCallbacks = [];
      });
    },
    fail: res => {
      console.log('获取用户信息失败');
    }
  })
}
/**
 * 第一次去登录
 */
function bindGetUserInfo(e) {
  var app = getApp();
  var that = this;
  var userInfo = e.detail.userInfo;
  if (!userInfo) {
    return wx.showToast({
      title: '授权失败，请稍后重试！',
      icon: 'none'
    });
  }
  wx.showLoading({
    title: '授权中~',
  })
  sendQuery(e.detail, function(ret) {
    wx.hideLoading();
    app.globalData.userInfo = ret.data.data;
    for (var i in app.loginCallbacks) {
      var Callback = app.loginCallbacks[i]
      typeof Callback == 'function' ? Callback() : ''
    }
    app.loginCallbacks = [];
    wx.showToast({
      title: '授权成功！',
      icon: 'none'
    })
    that.setData({
      ismask: false
    });
  }, function(data) {
    wx.hideLoading();
    wx.showToast({
      title: data.msg,
      icon: 'none'
    })
  })
}

function sendQuery(res, success, error) {
  var app = getApp();
  var url = http.portUrl + app.globalData.registerUrl;
  var data = {
    'rawData': res.rawData,
    'signature': res.signature,
    'encryptedData': res.encryptedData,
    'iv': res.iv
  }
  http.post(url, data, success, error);
}
/**
 * @params doauth  注册的两种方法，当用户已授权
 * @params error    注册的两种方法，当用户未授权或拒绝授权
 */
function login(doauth, notauth) {
  var app = getApp();
  var url = http.portUrl + app.globalData.loginUrl;
  wx.login({
    success: res => {
      var code = res.code;
      //用code到后台登录
      http.post(url, {
        code: code
      }, function(ret) {
        if (ret.statusCode == 200 && ret.data.code == 200) {
          wx.setStorageSync('userId', ret.data.data.id)
          //登录成功，用户无感知
          app.globalData.userInfo = ret.data.data;
          for (var i in app.loginCallbacks) {
            var Callback = app.loginCallbacks[i]
            typeof Callback == 'function' ? Callback() : ''
          }
          app.loginCallbacks = [];
        } else if (ret.data.code == 403) {
          //去注册
          wx.getSetting({
            success: res => {
              if (res.authSetting['scope.userInfo']) {
                // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
                typeof doauth === 'function' ? doauth() : '';
              } else {
                typeof notauth === 'function' ? notauth() : '';
                // console.log('没有授权');
              }
            },
            fail: res => {
              typeof notauth === 'function' ? notauth() : '';
              //console.log('拒绝授权');
            }
          })
        } else {
          app.showToast({
            title: '网络开小差去了',
            icon: 'warning'
          })
        }
      })
    }
  });
}