function showToast(obj) {
  var obj = sentence(obj);
  return wx.showToast(obj);
}

function hideToast(obj) {
  return wx.hideToast(obj);
}
/**
 * 生成指定范围随机数
 */
function randomFrom(lowerValue, upperValue) {
  return Math.floor(Math.random() * (upperValue - lowerValue + 1) + lowerValue);
}

function sentence(obj) {
  var app = getApp();
  var tips_list = app.globalData.tips_list;
  var identifier;
  if(typeof obj == 'string'){
     identifier = obj;
     obj = {};
  }else{
    identifier = obj.identifier;
  }
  if (obj.icon == 'warning') {
      obj.image = '/images/warning.png';
  }
  if(obj.icon == 'success'){
    obj.image = '/images/success.png';
  }
  if(obj.icon == 'error'){
    obj.image = '/images/error.png';
  }
  if (tips_list && identifier && tips_list[identifier]) {
    var len = tips_list[identifier].length;
    var ran = randomFrom(0, len - 1);
    obj.title = tips_list[identifier][ran];
    if (!obj.icon) {
      obj.icon = 'none';
    }
  } else if (!obj.icon && !obj.title) {
     obj.icon = 'loading';
    
  } else if (!obj.icon && obj.title){
    obj.icon = 'none'
  }
  if (obj.title && obj.title.length > 7 ){
    obj.icon = 'none';
    obj.image = '';
  }
  return obj;
}

module.exports = {
  showToast: showToast,
  hideToast: hideToast,
}