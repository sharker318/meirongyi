const COOKIE_KEY = '__cookie__';
export default {
  getCookie(key, def) {
    key = 'cookie_' + key;
    var obj = wx.getStorageSync(key);
    if (obj == undefined) {
      return def ? def : null;
    }
    var date = new Date();
    var time = date.getTime();
    if (obj.expires > time) {
      return obj.val;
    }
    wx.removeStorageSync(key);
    return def ? def : null;
  },

  setCookie(key, val, expires) {
    key = 'cookie_' + key;
    if (val === undefined || val === null || val === '') return wx.removeStorageSync(key);
    expires = expires ? expires : 365 * 24 * 3600; //存储到秒
    var date = new Date();
    expires = date.getTime() + expires * 1000;
    var obj = {
      expires: expires,
      val: val
    };
    wx.setStorageSync(key, obj);
  },

  removeCookie(key) {
    key = 'cookie_' + key;
    wx.removeStorageSync(key);
  },
  /**
   * 解析并后端设置cookie
   * @params cookies string
   */
  normalizeUserCookie(cookies) {
    if (!cookies) return false;
    let __cookies = this.getCookie(COOKIE_KEY) || {};
    (cookies.match(/([\w\-.]*)=([^\s=]+);/g) || []).forEach((str) => {
      if (str !== 'path=/;') {
        var item = str.split('=');
        __cookies[item[0]] = item[1];
      }
    });

    this.setCookie(COOKIE_KEY, __cookies);
    return true;
  },
  /**
   * 清除用户cookie
   */
  cleanUserCookie() {
    this.removeCookie(COOKIE_KEY);
  },
  getUserCookie(){
    return this.getCookie(COOKIE_KEY);
  }

}